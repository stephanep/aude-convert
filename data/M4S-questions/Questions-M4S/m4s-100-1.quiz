<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>2</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Pythagore</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On considère un repère orthonormé <sc:textLeaf role="mathtex">(O, \overrightarrow{u_x}, \overrightarrow{u_y}, \overrightarrow{u_z})</sc:textLeaf>. Un oiseau a un déplacement de 2km selon la direction <sc:textLeaf role="mathtex">\overrightarrow{u_x}</sc:textLeaf>, de 1 km dans le sens opposé à la direction <sc:textLeaf role="mathtex">\overrightarrow{u_y}</sc:textLeaf>, mais on ne connaît pas son déplacement selon selon la direction <sc:textLeaf role="mathtex">\overrightarrow{u_z}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">A quelle distance de sa position initiale se trouve t-il au final?</sc:inlineStyle></sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">à plus de 2 km</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">au moins <sc:textLeaf role="mathtex">\sqrt{5}</sc:textLeaf> km</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">On ne peut pas savoir</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">On ne peut pas lui donner de valeur numérique, mais on sait qu'elle est plus grande que <sc:textLeaf role="mathtex">\sqrt{5}</sc:textLeaf> km</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">à <sc:textLeaf role="mathtex">(\sqrt{5} + |z| )</sc:textLeaf>km</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\sqrt{5 + z^2}</sc:textLeaf> km n'est pas égal à <sc:textLeaf role="mathtex">\sqrt{5} + |z|</sc:textLeaf> km</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">à <sc:textLeaf role="mathtex">(\sqrt{3} + |z|)</sc:textLeaf> km</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention, <sc:textLeaf role="mathtex">(-1)^2 = 1^2 = 1</sc:textLeaf> et <sc:textLeaf role="mathtex">\sqrt{3 + z^2}</sc:textLeaf> n'est pas égal à <sc:textLeaf role="mathtex">\sqrt{3 } + |z|</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1566824643376">vecteur colonne</comment></comment>-->
          <op:txt>
            <sc:para xml:space="preserve">Les composantes du vecteur déplacement <sc:textLeaf role="mathtex">\overrightarrow{v}</sc:textLeaf> sont <sc:textLeaf role="mathtex">\begin{pmatrix}2\\-1\\z\end{pmatrix}</sc:textLeaf> kms. Sa norme vaut donc <sc:textLeaf role="mathtex">||\overrightarrow{ v} || = \sqrt{2^2+(-1)^2+z^2} = \sqrt{5 + |z|^2}</sc:textLeaf> km qui est supérieur à <sc:textLeaf role="mathtex">\sqrt{5}</sc:textLeaf> km si la composante suivant <sc:textLeaf role="mathtex">z</sc:textLeaf> est non nulle.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
