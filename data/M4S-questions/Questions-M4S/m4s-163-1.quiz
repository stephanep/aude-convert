<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3">
    <op:exeM>
      <sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Étudier la dépendance d'une grandeur</sp:title>
      <sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#phys-mathphys-</sp:themeLicence>
      <ns0:themeLicence xmlns:ns0="http://www.utc.fr/ics/scenari/v3/primitive">#math-</ns0:themeLicence>
      <sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">3</sp:level>
      <sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L0</sp:educationLevel>
      <sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF - fonction trigo-graphe</sp:keywd>
              <sp:keywd>Contexte - maths</sp:keywd>
              <sp:keywd>Équation caractéristique</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Richard Dupont</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
          <op:txt>
            <sc:para xml:space="preserve">Soit la fonction f définie par f(x) = 3 cos (x).</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est la courbe représentative de f ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" sc:refUri="images/enonce-m4s-163-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">courbe 1</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">courbe 2</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">L'amplitude est  1,5 sur ce graphe. Confusion avec la différence du maximum et du minimum de la fonction.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">courbe 3</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">L'amplitude est  1,5 sur ce graphe. Confusion avec la différence du maximum et du minimum de la fonction. De plus la période n'est pas conforme.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">courbe 4</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">La période de la fonction est <sc:textLeaf role="mathtex">2\pi</sc:textLeaf> et ne correspond pas à celle de ce graphe.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
          <op:txt>
            <sc:para xml:space="preserve">On regarde le graphe <sc:textLeaf role="mathtex">y = 3 \cos(x)</sc:textLeaf></sc:para>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve"><sc:textLeaf role="mathtex">\cos(x)</sc:textLeaf> prend ses valeurs entre -1 et 1. La courbe correspondante doit donc être comprise entre -3 et 3 : c'est le cas des courbes 1 et 4</sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">La fonction cosinus est périodique de période <sc:textLeaf role="mathtex">2\pi</sc:textLeaf>. La courbe correspondante doit donc se répéter tous les <sc:textLeaf role="mathtex">2\pi</sc:textLeaf> : c'est le cas des courbes 1 et 2. Les courbes 3 et 4 se répètent à l'identique tous les <sc:textLeaf role="mathtex">\pi</sc:textLeaf>. </sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">La fonction cosinus est maximale en <sc:textLeaf role="mathtex">x=0</sc:textLeaf>, ce qui est le cas de toutes les courbes présentées ici. </sc:para>
              </sc:listItem>
            </sc:itemizedList>
            <sc:para xml:space="preserve">La courbe solution est donc la courbe 4. </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" sc:refUri="images/m4s-163-FB.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
