<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Coordonnées</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On considère la base <sc:textLeaf role="mathtex">(\vec{i}, \vec{j})</sc:textLeaf> du plan et <sc:textLeaf role="mathtex">a,b,c,d</sc:textLeaf> sont quatre nombres réels.</sc:para>
            <sc:para xml:space="preserve">On définit les vecteurs <sc:textLeaf role="mathtex">\overrightarrow{AB}= a\vec{i}+ b\vec{j}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{BC}= c\vec{i}+d\vec{j}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est l'expression qui permet d'obtenir les coordonnées du vecteur <sc:textLeaf role="mathtex">\overrightarrow{AC}</sc:textLeaf> </sc:inlineStyle>
              <sc:inlineStyle role="emp">?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{AC}= (a+c)\vec{i}+ (b+d)\vec{j}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{AC}= (a+d)\vec{i}+ (b+c)\vec{j}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{AC}= (a.c)\vec{i}+ (b.d)\vec{j}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Définition du produit scalaire</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{AC}= (a+b)\vec{i}+ (c+d)\vec{j}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Erreur dans les lettres</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{AC}= (a-c)\vec{i}+ (b-d)\vec{j}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Définition du produit vectoriel</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On développe la somme avec les vecteurs de base :</sc:para>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\overrightarrow{AC}= \overrightarrow{AB}+\overrightarrow{BC}= a\vec{i}+ b\vec{j}+c\vec{i}+d\vec{j}</sc:textLeaf>
            </sc:para>
            <sc:para xml:space="preserve">Puis on factorise :</sc:para>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\overrightarrow{AC}= (a+c)\vec{i} + (b+d)\vec{j}</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
