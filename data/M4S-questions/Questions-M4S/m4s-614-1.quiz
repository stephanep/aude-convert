<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>contexte physique</sp:keywd>
              <sp:keywd>Projection</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">A. Caussarieu</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Un pendule est soumis à la force de tension du fil <sc:textLeaf role="mathtex">\overrightarrow T</sc:textLeaf> et à son poids <sc:textLeaf role="mathtex">\overrightarrow P</sc:textLeaf>. On veut décrire ces forces dans la base <sc:textLeaf role="mathtex">\left( \overrightarrow{e_r},\overrightarrow{e_\theta}\right)</sc:textLeaf>. </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-613.png">
          <op:resInfoM/>
        </sp:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est la composante de <sc:textLeaf role="mathtex">\overrightarrow P</sc:textLeaf> selon <sc:textLeaf role="mathtex"> \overrightarrow{e_r}</sc:textLeaf> ? </sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">P\cos\theta</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">-P\cos\theta</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Le poids agit "dans le même sens" que  <sc:textLeaf role="mathtex"> \overrightarrow{e_r}</sc:textLeaf> </sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">-P\sin\theta</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Dessiner le triangle rectangle dont l'hypothénuse est formé par P et l'autre côté par le projeté de P sur  <sc:textLeaf role="mathtex"> \overrightarrow{e_r}</sc:textLeaf> pour savoir s'il faut utiliser sinus ou cosinus. </sc:para>
            <sc:para xml:space="preserve">Le poids agit "dans le même sens" que  <sc:textLeaf role="mathtex"> \overrightarrow{e_r}</sc:textLeaf></sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">P\sin\theta</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Dessiner le triangle rectangle dont l'hypothénuse est formé par P et l'autre côté par le projeté de P sur  <sc:textLeaf role="mathtex"> \overrightarrow{e_r}</sc:textLeaf> pour savoir s'il faut utiliser sinus ou cosinus. </sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">1) On commence par dessiner le triangle rectangle dont l'hypoténuse est le vecteur <sc:textLeaf role="mathtex">\overrightarrow{P}</sc:textLeaf> et dont l'un des côtés est la projection de <sc:textLeaf role="mathtex">\overrightarrow{P}</sc:textLeaf> sur <sc:textLeaf role="mathtex">\overrightarrow{e_r}</sc:textLeaf>. </sc:para>
            <sc:para xml:space="preserve">2) On identifie ensuite le côté opposé : <sc:textLeaf role="mathtex">P\sin\theta</sc:textLeaf> et le côté adjacent :<sc:textLeaf role="mathtex">P\cos\theta</sc:textLeaf>. </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/m4s-614-corrige.png">
          <op:resInfoM/>
        </sp:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">3) L'énoncé nous demande la composante selon <sc:textLeaf role="mathtex">\overrightarrow{e_r}</sc:textLeaf>, donc la norme de cette composante est  <sc:textLeaf role="mathtex">P\cos\theta</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">4) Le poids agit dans le même sens que  <sc:textLeaf role="mathtex">\overrightarrow{e_r}</sc:textLeaf>, il faut donc mettre un signe plus. </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
