<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Déterminer l'expression littérale d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Système d'équations</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Soit <sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{v}</sc:textLeaf> de coordonnées respectives dans <sc:textLeaf role="mathtex">R^2</sc:textLeaf> <sc:textLeaf role="mathtex">\begin{pmatrix}3a\\-2\end{pmatrix}</sc:textLeaf> et <sc:textLeaf role="mathtex">\begin{pmatrix}-6\\2b\end{pmatrix}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">a</sc:textLeaf> et <sc:textLeaf role="mathtex">b</sc:textLeaf> étant des nombres réels.</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Quel couple de valeurs <sc:textLeaf role="mathtex">(a,b)</sc:textLeaf> permet d'obtenir que la somme des vecteurs soit le vecteur nul <sc:textLeaf role="mathtex">\overrightarrow{0}</sc:textLeaf> ?</sc:inlineStyle></sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\begin{pmatrix}2\\1\end{pmatrix}</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\begin{pmatrix}-2\\-1\end{pmatrix}</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Si on veut vérifier ce résultat essayons 3x(-2)-6=-12, ce qui est différent de 0 donc faux (il n'est même pas nécessaire de vérifier l'ordonnée).</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\begin{pmatrix}-2\\1\end{pmatrix}</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Si on veut vérifier ce résultat essayons 3x(-2)-6=-12, ce qui est différent de 0 donc faux (il n'est même pas nécessaire de vérifier l'ordonnée).</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\begin{pmatrix}2\\-1\end{pmatrix}</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Si on veut vérifier ce résultat essayons 3x2-6=0, l'abscisse est correcte, il faut vérifier l'ordonnée et -2+2x(-1)=-4 ce qui est différent de 0 donc faux.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:textLeaf role="mathtex">\overrightarrow{u}+\overrightarrow{v}=\overrightarrow{0}</sc:textLeaf> si toutes les coordonnées du vecteurs <sc:textLeaf role="mathtex">\overrightarrow{u}+\overrightarrow{v}</sc:textLeaf> sont nulles c'est-à-dire <sc:textLeaf role="mathtex">3a-6=0</sc:textLeaf> et <sc:textLeaf role="mathtex">-2+2b=0</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Ainsi la somme des vecteurs est nulle si <sc:textLeaf role="mathtex">a=2</sc:textLeaf> et <sc:textLeaf role="mathtex">b=1</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
