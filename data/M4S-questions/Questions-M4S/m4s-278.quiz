<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>vecteurs coordonnées caractéristiques</sp:keywd>
              <sp:keywd>contexte physique</sp:keywd>
              <sp:keywd>niveau 1</sp:keywd>
              <sp:keywd>Définition d'un vecteur</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - H. Dechelette</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Calculatrice autorisée</sc:para>
            <sc:para xml:space="preserve">Le champ de pesanteur terrestre est modélisé par un vecteur<sc:textLeaf role="mathtex"> \vec{g}</sc:textLeaf> dont la direction est verticale, le sens vers les bas et la norme vaut environ 10 m/s²</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Comment peut on écrire la coordonnée <sc:textLeaf role="mathtex">g_z</sc:textLeaf> de ce vecteur ?</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">g_z = 10 m/s²</sc:textLeaf> dans un repère dont l'axe vertical (Oz) est orienté vers le haut</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Le sens du vecteur <sc:textLeaf role="mathtex"> \vec{g}</sc:textLeaf> est vers le bas donc sa coordonnée est négative dans un repère pointant vers le haut.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">g_z = -10 m/s²</sc:textLeaf> dans un repère dont l'axe vertical (Oz) est orienté vers le haut</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">g_z = 10 m/s²</sc:textLeaf> dans un repère dont l'axe vertical (Oz) est orienté vers le bas</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">g_z = - 10 m/s²</sc:textLeaf> dans un repère dont l'axe vertical (Oz) est orienté vers le bas</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Le sens du vecteur <sc:textLeaf role="mathtex"> \vec{g}</sc:textLeaf> est vers le bas donc sa coordonnée est positive dans un repère pointant vers le bas.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Les coordonnées du vecteur doivent tenir compte du repère choisi.</sc:para>
            <sc:para xml:space="preserve">Un vecteur aura une coordonnée positive dans un repère orienté dans le même sens que le vecteur et une coordonnée négative dans un repère orienté dans l'autre sens.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
