<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Déterminer l'expression littérale d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>2</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Somme de vecteurs</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelles sont les affirmations correctes parmi les phrases ci dessous ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-204-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_1} + \overrightarrow{U_3}</sc:textLeaf> a une coordonnée négative suivant l'axe des abscisses</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_3} + \overrightarrow{U_4}</sc:textLeaf> ont une coordonnée positive suivant l'axe des abscisses</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve">La coordonnée de <sc:textLeaf role="mathtex"> \overrightarrow{U_3}</sc:textLeaf> selon l'axe des abscisses est négative</sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">La coordonnée de <sc:textLeaf role="mathtex"> \overrightarrow{U_4}</sc:textLeaf> selon l'axe des abscisses est négative</sc:para>
              </sc:listItem>
            </sc:itemizedList>
            <sc:para xml:space="preserve">Donc la coordonnée de <sc:textLeaf role="mathtex">\overrightarrow{U_3} + \overrightarrow{U_4}</sc:textLeaf> selon l'axe des abscisse est négative.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_3} - \overrightarrow{U_2}</sc:textLeaf> a une coordonnée nulle suivant l'axe des abscisses.</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve">La coordonnée de <sc:textLeaf role="mathtex"> \overrightarrow{U_3}</sc:textLeaf> selon l'axe des abscisses est négative</sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">La coordonnée de <sc:textLeaf role="mathtex"> \overrightarrow{U_2}</sc:textLeaf> selon l'axe des abscisses est positive, donc la coordonnée de <sc:textLeaf role="mathtex"> -\overrightarrow{U_2}</sc:textLeaf> est négative</sc:para>
              </sc:listItem>
            </sc:itemizedList>
            <sc:para xml:space="preserve">Et donc la coordonnée de <sc:textLeaf role="mathtex">\overrightarrow{U_3} + \overrightarrow{U_4}</sc:textLeaf> selon l'axe des abscisse est négative.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_2} - \overrightarrow{U_4}</sc:textLeaf> a une coordonnée positive suivant l'axe des abscisses.</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_2} - \overrightarrow{U_1}</sc:textLeaf> a une coordonnée négative suivant l'axe des abscisses.</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve">La coordonnée de <sc:textLeaf role="mathtex"> \overrightarrow{U_2}</sc:textLeaf> selon l'axe des abscisses est positive</sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">La coordonnée de <sc:textLeaf role="mathtex"> \overrightarrow{U_1}</sc:textLeaf> selon l'axe des abscisses est nulle</sc:para>
              </sc:listItem>
            </sc:itemizedList>
            <sc:para xml:space="preserve">Donc la coordonnée de <sc:textLeaf role="mathtex">\overrightarrow{U_2} - \overrightarrow{U_1}</sc:textLeaf> selon l'axe des abscisses est positive.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">À savoir :</sc:inlineStyle> La composante horizontale d'une somme de vecteurs est égale à la somme des composantes horizontales des vecteurs.</sc:para>
            <sc:para xml:space="preserve">Mathématiquement cela se traduit par : <sc:textLeaf role="mathtex">\left(\overrightarrow{A+B}\right)_x=A_x+B_x</sc:textLeaf></sc:para>
            <sc:para xml:space="preserve">Plus généralement, l'opération correspondant à prendre la composante horizontale (ou verticale) d'un vecteur est une application linéaire :<sc:textLeaf role="mathtex"> \left(\overrightarrow{A}+\lambda\overrightarrow{B}\right)_x=A_x+\lambda\cdot B_x</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
