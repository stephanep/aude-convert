<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Déterminer l'expression littérale d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>2</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Produit scalaire</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">

Un hélicoptère peut se déplacer horizontalement ou verticalement dans un plan vertical. On note <sc:textLeaf role="mathtex">\overrightarrow{H}</sc:textLeaf> son déplacement vertical orienté vers le haut, <sc:textLeaf role="mathtex">\overrightarrow{B}</sc:textLeaf> son déplacement vertical orienté vers le bas, <sc:textLeaf role="mathtex">\overrightarrow{D}</sc:textLeaf> son déplacement horizontal orienté à droite et <sc:textLeaf role="mathtex">\overrightarrow{G}</sc:textLeaf> son déplacement horizontal orienté à gauche.</sc:para>
            <sc:para xml:space="preserve">La gravitation exerce une force <sc:textLeaf role="mathtex">\overrightarrow{P}</sc:textLeaf> verticale orientée vers le bas.</sc:para>
            <sc:para xml:space="preserve">Le travail d'une force <sc:textLeaf role="mathtex">\overrightarrow{P}</sc:textLeaf> sur un trajet <sc:textLeaf role="mathtex">\overrightarrow{T}</sc:textLeaf> est donnée par le produit scalaire <sc:textLeaf role="mathtex">\overrightarrow{P}.\overrightarrow{T}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Dans quel(s) cas le travail de la force de gravité est-il nul ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-117-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">Dans le cas d'un déplacement vers le haut <sc:textLeaf role="mathtex">\overrightarrow{H}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Dans ce cas, le travail sera strictement négatif.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">Dans le cas d'un déplacement vers la droite <sc:textLeaf role="mathtex">\overrightarrow{D}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">Dans le cas d'un déplacement vers la gauche <sc:textLeaf role="mathtex">\overrightarrow{G}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">Dans le cas d'un déplacement vers le bas<sc:textLeaf role="mathtex"> \overrightarrow{B}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Dans ce cas, le travail sera strictement positif.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Le produit scalaire de deux vecteurs est nul lorsque les deux vecteurs sont orthogonaux. Dans ce cas, la gravité exerce une force verticale vers le bas. Si le travail de la gravité pendant le déplacement est nul, c'est que le déplacement se fait de manière orthogonale à la force, c'est à dire horizontalement vers la droite ou vers la gauche.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
