<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Estimer les paramètres d'un modèle</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF - fonction affine-équation droite-graphe</sp:keywd>
              <sp:keywd>Contexte - maths</sp:keywd>
              <sp:keywd>Équation caractéristique</sp:keywd>
              <sp:keywd>Pente</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Richard Dupont</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quel est le calcul  qui mène au résultat du coefficient directeur m de la droite représentée ci-dessous ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-467-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="ind"/> <sc:textLeaf role="mathtex">m= \frac{1-2}{2-(-1)}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Inversion du quotient. Calculer avec : <sc:textLeaf role="mathtex">m=\frac{\Delta y}{\Delta x}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="ind"/>
<sc:textLeaf role="mathtex">m= \frac{-1-2}{1-2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="ind"/>
<sc:textLeaf role="mathtex">m= \frac{2}{(-1)}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Il ne faut pas faire le quotient des ordonnées des 2 points repérés sur la droite - Revoir l'expression du coefficient directeur.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="ind"/>
<sc:textLeaf role="mathtex">m= \frac{2}{1}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Il ne faut pas faire le quotient des abscisses des 2 points repérés sur la droite - Revoir l'expression du coefficient directeur.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">m= \frac{2-(-1)}{2-1}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Erreur de signe pour le calcul du dénominateur - Revoir l'expression du coefficient directeur</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1567001260979">à finir de rédiger -&gt; AN</comment></comment>-->
          <op:txt>
            <sc:para xml:space="preserve">On a une fonction linéaire  dont m est le coefficient directeur (pente) qu'il faut calculer.</sc:para>
            <sc:para xml:space="preserve">Il faut calculer cette pente à l'aide des coordonnées des 2 points présentés sur le schéma. Si la droite  de pente m passe par deux points A<sc:textLeaf role="mathtex">\ (x_A ;y_A)</sc:textLeaf> et B<sc:textLeaf role="mathtex">\ (x_B ;y_B)</sc:textLeaf>, par définition :  <sc:textLeaf role="mathtex">m=\frac{y_B-y_A}{ x_B-x_A}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Ainsi on a : <sc:textLeaf role="mathtex">m=\frac{-1-2}{2-1}=\frac{-3}{1}=-3</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
