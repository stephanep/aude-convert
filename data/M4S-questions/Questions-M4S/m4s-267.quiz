<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Représenter l'évolution d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>LOG caracteristiques</sp:keywd>
              <sp:keywd>contexte physique</sp:keywd>
              <sp:keywd>niveau 3</sp:keywd>
              <sp:keywd>Équation caractéristique</sp:keywd>
              <sp:keywd>Propriétés des fonctions</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - H. Dechelette</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">La 3e loi de Kepler s'exprime ainsi : Le carré de la période <sc:textLeaf role="mathtex">(T)</sc:textLeaf> de révolution d'un satellite divisé par le cube du demi-grand axe de l'ellipse <sc:textLeaf role="mathtex">(a)</sc:textLeaf> parcourue par le satellite est une constante <sc:textLeaf role="mathtex">(C)</sc:textLeaf> pour un astre attracteur donné. Cela peut aussi s'écrire : <sc:textLeaf role="mathtex">{T^2 \over a^3} = C</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">Pour vérifier la validité de cette formule (avec des valeurs expérimentales) , on exprime la relation autrement :</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex"> \log(T ) = {3 \over 2}\log( a) +{1 \over 2 }\log (C)</sc:textLeaf>
 et on trace le graphe <sc:textLeaf role="mathtex">\log (T)</sc:textLeaf> en fonction de <sc:textLeaf role="mathtex"> \log (a)</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Si la loi est vérifiée, alors quel est le type de tracé obtenu parmi les propositions suivantes?</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">la courbe obtenue est une droite qui ne passe pas par l'origine</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">La fonction <sc:textLeaf role="mathtex">\log (T)</sc:textLeaf> n'est pas proportionnelle à la fonction de <sc:textLeaf role="mathtex"> \log (a)</sc:textLeaf>.</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">la courbe obtenue est celle de la fonction <sc:textLeaf role="mathtex">f(x) = \log (x)</sc:textLeaf></sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">La fonction <sc:textLeaf role="mathtex">\log (T)</sc:textLeaf> n'est pas proportionnelle à la fonction de <sc:textLeaf role="mathtex"> \log (a)</sc:textLeaf> comme proposé dans cette réponse</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">La courbe obtenue est une droite et son coefficient directeur vaut 1,5</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Raisonnement </sc:inlineStyle>: On a l'expression <sc:textLeaf role="mathtex">\log(T ) = {3 \over 2} \log( a) +{1 \over 2 }\log (C)</sc:textLeaf> et on trace <sc:textLeaf role="mathtex">\log(T)</sc:textLeaf> en fonction de <sc:textLeaf role="mathtex">\log(C)</sc:textLeaf>. </sc:para>
            <sc:para xml:space="preserve">On peut construire l'analogie suivante : </sc:para>
            <sc:table role="">
              <sc:column width="50"/>
              <sc:column width="50"/>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Situation de chimie présentée ici</sc:para>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">\log(T ) = {3 \over 2} \log( a) +{1 \over 2 }\log (C)</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">Situation typique de mathématiques</sc:para>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">y=b+a\cdot x</sc:textLeaf>
                  </sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">\log(T)</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">y</sc:textLeaf>
                  </sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">\log(C)</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve"><sc:textLeaf role="mathtex">x</sc:textLeaf> : variable</sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">\frac{1}{2}</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve"><sc:textLeaf role="mathtex">a</sc:textLeaf> : pente</sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">\frac{3}{2}\log(a)</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve"><sc:textLeaf role="mathtex">b</sc:textLeaf> : ordonnée à l'origine</sc:para>
                </sc:cell>
              </sc:row>
            </sc:table>
            <sc:para xml:space="preserve">On reconnaît donc bien l'équation d'une droite dont l'ordonnée à l'origine est non nulle et dont la pente vaut 1/2. </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
