<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1566809012354">Manque feedback</comment></comment>-->
    <op:exeM>
      <sp:title>Modéliser une situation</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Contexte - maths</sp:keywd>
              <sp:keywd>Modélisation</sp:keywd>
              <sp:keywd>Proportionnalité</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - H. Delanoë-Ayari</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Le prix d'une voiture est de 10000 euros  avant de subir en décembre une hausse de 11% puis en janvier une baisse de 11%.</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est le prix en janvier de la voiture?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">plus de 10000 euros</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Le prix de la voiture est plus élevé en janvier qu'en décembre. La baisse de prix est donc pratiquée sur un prix plus élevé que celui auquel est appliqué la hausse. Ce qui fait que le prix sera inférieur en janvier par rapport au prix de départ.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">10000 euros</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Le prix de la voiture est plus élevé en janvier qu'en décembre. La baisse de prix est donc pratiquée sur un prix plus élevé que celui auquel est appliqué la hausse. Ce qui fait que le prix sera inférieur en janvier par rapport au prix de départ.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">moins de 10000 euros</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">On ne peut pas savoir sans faire le calcul</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Le prix de la voiture est plus élevé en janvier qu'en décembre. La baisse de prix est donc pratiquée sur un prix plus élevé que celui auquel est appliqué la hausse. Ce qui fait que le prix sera inférieur en janvier par rapport au prix de départ.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1568211034228">Harmoniser avec exercice 587-1</comment></comment>-->
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Raisonnement : </sc:inlineStyle>
            </sc:para>
            <sc:para xml:space="preserve">Le prix de la voiture est plus élevé en janvier qu'en décembre. La baisse de prix est donc pratiquée sur un prix plus élevé que celui auquel est appliqué la hausse. Ce qui fait que le prix sera inférieur en janvier par rapport au prix de départ.</sc:para>
            <sc:para xml:space="preserve">Prouvons le par des calculs.</sc:para>
            <sc:para xml:space="preserve">On appelle <sc:textLeaf role="mathtex">P_1</sc:textLeaf> le prix de l'article avant la hausse des prix, <sc:textLeaf role="mathtex">P_2</sc:textLeaf> le prix de l'article après la hausse des prix <sc:textLeaf role="mathtex">P_3</sc:textLeaf> le prix de l'article après la baisse des prix. L'énoncé se traduit alors mathématiquement par : <sc:textLeaf role="mathtex">P_2=P_1+P_1\times\frac{11}{100}</sc:textLeaf> et  <sc:textLeaf role="mathtex">P_3=P_2-P_2\times\frac{11}{100}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Calcul :</sc:inlineStyle> On a donc <sc:textLeaf role="mathtex">P_2=P_1(1+0,11)=P_1\times1,11</sc:textLeaf> et  <sc:textLeaf role="mathtex">P_3=P_2(1-0,11)=P_2\times0,89</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Application numérique :</sc:inlineStyle> <sc:textLeaf role="mathtex">P_1=10000 \mbox{ euros}\times1,11=11100\ \mbox{euros}</sc:textLeaf> et <sc:textLeaf role="mathtex">P_3=11100 \mbox{ euros}\times0,89=9879\ \mbox{euros}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Le prix final est donc inférieur à 10000 euros après les différentes variations du prix.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
