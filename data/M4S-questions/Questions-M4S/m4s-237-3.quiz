<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Estimer les paramètres d'un modèle</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF - fonction affine-équation droite-graphe</sp:keywd>
              <sp:keywd>Contexte - physique</sp:keywd>
              <sp:keywd>Équation caractéristique</sp:keywd>
              <sp:keywd>Pente</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Richard Dupont</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">A l'aide d'un consomètre, on a mesuré l'énergie <sc:textLeaf role="mathtex">E</sc:textLeaf> consommée par une cafetière électrique au cours de son fonctionnement (durée <sc:textLeaf role="mathtex">t</sc:textLeaf>).</sc:para>
            <sc:para xml:space="preserve">On a ainsi pu tracer le graphe (figure suivante) de la fonction <sc:textLeaf role="mathtex">E</sc:textLeaf> de variable <sc:textLeaf role="mathtex">t</sc:textLeaf>. <sc:textLeaf role="mathtex">E(t) = P \times t</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-237-3.png">
          <op:resInfoM/>
        </sp:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle valeur de la puissance de cet appareil peut-on en déduire ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">
P = 2000 \ \mbox{W}</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">
P = 0,0005 \ \mbox{W}</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Pente inversée. La pente est la valeur du quotient de la variation verticale <sc:textLeaf role="mathtex">(E)</sc:textLeaf> sur la variation horizontale <sc:textLeaf role="mathtex">(t)</sc:textLeaf> et non l'inverse.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">
P = 8000 \ \mbox{W}</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">La droite passe par le point (2 ; 4000). Calculer le quotient et non le produit des coordonnées.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="exp"/><sc:textLeaf role="mathtex">P=\frac{1}{2000}\ \mbox{W}</sc:textLeaf></sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Pente inversée. La pente est la valeur du quotient de la variation verticale <sc:textLeaf role="mathtex">(E)</sc:textLeaf> sur la variation horizontale <sc:textLeaf role="mathtex">(t)</sc:textLeaf> et non l'inverse.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1567001231184">à finir de rédiger -&gt; AN
fractions en fractions latex</comment></comment>-->
          <op:txt>
            <sc:para xml:space="preserve"><sc:textLeaf role="mathtex">E</sc:textLeaf> est une fonction linéaire du temps <sc:textLeaf role="mathtex">t</sc:textLeaf> dont la puissance <sc:textLeaf role="mathtex">P</sc:textLeaf> est le coefficient directeur (pente) qu'il faut calculer : <sc:textLeaf role="mathtex">E(t) = P \times t</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Il faut calculer cette pente à l'aide des coordonnées des 2 points présentés sur le schéma. Si la droite  de pente <sc:textLeaf role="mathtex">P</sc:textLeaf> passe par deux points <sc:textLeaf role="mathtex">A(t_{A} ; E_{A})</sc:textLeaf> et <sc:textLeaf role="mathtex">B(t_{B} ; E_{B})</sc:textLeaf>, par définition :  <sc:textLeaf role="mathtex">P=\frac{E_B-E_A}{ t_B-t_A}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Ainsi on a : <sc:textLeaf role="mathtex">P=\frac{4000-0}{2-0}=\frac{4000}{2}=2000 \ \mbox{W}</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
