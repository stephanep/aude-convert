<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3">
    <op:exeM>
      <sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#phys-mathphys-</sp:themeLicence>
      <ns0:themeLicence xmlns:ns0="http://www.utc.fr/ics/scenari/v3/primitive">#phys-</ns0:themeLicence>
      <sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">3</sp:level>
      <sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L0</sp:educationLevel>
      <sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Projection</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Que vaut la composante <sc:textLeaf role="mathtex">F_y</sc:textLeaf> de la force <sc:textLeaf role="mathtex">\overrightarrow{F}</sc:textLeaf> sur l'axe des ordonnées ?</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" sc:refUri="images/enonce-m4s-138-1-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\cos(\alpha) * ||\overrightarrow{F}||</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">C'est le côté opposé qui nous intéresse ici, pas le côté adjacent !</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">||\overrightarrow{F}|| / \cos(\alpha)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention de ne pas confondre la longueur du côté adjacent et celle de l'hypoténuse dans ta formule.</sc:para>
            <sc:para xml:space="preserve">Attention de ne pas confondre le cosinus et le sinus.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\sin(\alpha) * ||\overrightarrow{F}||</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Correct !</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">||\overrightarrow{F}|| / \sin(\alpha)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention de ne pas confondre la longueur du côté opposé et de l'hypoténuse dans la formule.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
          <op:txt>
            <sc:para xml:space="preserve">En physique  la coordonnée de <sc:textLeaf role="mathtex">\overrightarrow{F}</sc:textLeaf> sur l'axe des ordonnées  est souvent appelée la composante <sc:textLeaf role="mathtex">F_y</sc:textLeaf> du vecteur <sc:textLeaf role="mathtex">\overrightarrow{F}</sc:textLeaf>. Sur le schéma suivant, on retrouve un triangle ABC rectangle en C. La valeur cherchée est AC. On a <sc:textLeaf role="mathtex">\sin(\alpha) = \frac{AC}{AB}</sc:textLeaf>. Donc <sc:textLeaf role="mathtex">AC = AB * \sin(\alpha)</sc:textLeaf>. De plus, <sc:textLeaf role="mathtex">AB = ||\overrightarrow{F}||</sc:textLeaf>. Donc la composante de <sc:textLeaf role="mathtex">\overrightarrow{F}</sc:textLeaf> sur l'axe des ordonnées vaut <sc:textLeaf role="mathtex">\sin(\alpha) * ||\overrightarrow{F}||</sc:textLeaf>. L'axe Oy est orienté et <sc:textLeaf role="mathtex">F_y</sc:textLeaf> est positif.</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" sc:refUri="images/enonce-m4s-138-1-1c.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
