<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Projection</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On se place dans l'espace avec un repère orthonormé <sc:textLeaf role="mathtex">(\overrightarrow{i}, \overrightarrow{j})</sc:textLeaf>. </sc:para>
            <sc:para xml:space="preserve">On définit le vecteur <sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> dont la coordonnée <sc:textLeaf role="mathtex">u_x</sc:textLeaf> est égale à 300. Ce vecteur fait un angle de 20° dans le sens trigonométrique avec l'axe <sc:textLeaf role="mathtex">O_x</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est la norme de <sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">environ 320</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">environ 282</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention à ne pas inverser le côté adjacent et l'hypoténuse dans la formule</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">environ 123</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention à ne pas inverser le côté adjacent et l'hypoténuse dans la formule et ne pas oublier de convertir l'angle de radians en degrés.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">environ 735</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention à l'unité de l'angle</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">environ 877</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention à ne pas confondre le sinus et le cosinus</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On dessine le vecteur à projeter à l'origine du repère. On reconnaît le triangle rectangle formé par le vecteur, son projeté orthogonal sur x et son projeté orthogonal sur y. Dans ce triangle rectangle : <sc:textLeaf role="mathtex">\cos(20°)= 300/ ||\overrightarrow{u} ||</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">Soit <sc:textLeaf role="mathtex">||\overrightarrow{u} ||= 300 / \cos(20°) = 320</sc:textLeaf> environ. Sur le schéma, on voit que la norme du vecteur est légèrement plus grande que sa projection.</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-090-1-1c.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
