<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Déterminer l'expression littérale d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>1</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Produit scalaire</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On considère deux vecteurs <sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{v}</sc:textLeaf> de norme non nulle. On appelle angle <sc:textLeaf role="mathtex">\alpha</sc:textLeaf> l'angle non orienté entre les vecteurs <sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{v}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Dans quel(s) cas a-t-on le produit scalaire <sc:textLeaf role="mathtex">\overrightarrow{u}.\overrightarrow{v}</sc:textLeaf> égal à 0?</sc:inlineStyle></sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{v}</sc:textLeaf> ont même direction et même sens</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Dans ce cas <sc:textLeaf role="mathtex">\cos(\alpha)=1</sc:textLeaf>, et le produit scalaire donne <sc:textLeaf role="mathtex">|| \overrightarrow{u} || .|| \overrightarrow{v} ||</sc:textLeaf> qui est non nul car les deux vecteurs sont de norme non nulle.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{v}</sc:textLeaf> ont même direction mais sont de sens opposé</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Dans ce cas <sc:textLeaf role="mathtex">\cos(\alpha)=-1</sc:textLeaf>, et le produit scalaire donne <sc:textLeaf role="mathtex">- || \overrightarrow{u} || .|| \overrightarrow{v} ||</sc:textLeaf>, qui est non nul car les deux vecteurs sont de norme non nulle.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{v}</sc:textLeaf> sont perpendiculaires</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Dans ce cas cos (\alpha)=0, et le produit scalaire vaut 0.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{u}=- \overrightarrow{v}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Dans ce cas <sc:textLeaf role="mathtex">\cos(\alpha)=-1</sc:textLeaf>, et le produit scalaire donne <sc:textLeaf role="mathtex">- || \overrightarrow{u} ||^2 ||</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{u} =\overrightarrow{v}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Dans ce cas le produit scalaire donne <sc:textLeaf role="mathtex"> || \overrightarrow{u} ||^2 car \cos(\alpha)=1</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Le produit scalaire  <sc:textLeaf role="mathtex">\overrightarrow{u}.\overrightarrow{v}</sc:textLeaf> est donné par   <sc:textLeaf role="mathtex">\overrightarrow{u}.\overrightarrow{v} ) = ||\overrightarrow{u}|| ||\overrightarrow{v} || \cos((\overrightarrow{u}, \overrightarrow{v}))  </sc:textLeaf> Comme  <sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{v}</sc:textLeaf> sont de norme non nulle, le seul cas où le produit scalaire s'annule est le cas où  <sc:textLeaf role="mathtex">\cos((\overrightarrow{u}, \overrightarrow{v})) = 0</sc:textLeaf>, cad le cas où les deux vecteurs sont orthogonaux</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
