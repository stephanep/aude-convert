<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Déterminer l'expression littérale d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>LOG caracteristiques</sp:keywd>
              <sp:keywd>contexte physique</sp:keywd>
              <sp:keywd>niveau 2</sp:keywd>
              <sp:keywd>Procédés réciproques</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - H. Dechelette</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">La cinétique d'une réaction chimique a été étudiée et sa vitesse suit la loi <sc:textLeaf role="mathtex"> v = k \times [A]^{2}[B]^{3}</sc:textLeaf> avec <sc:textLeaf role="mathtex">k</sc:textLeaf> constante</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Quelles sont les expressions correctes de <sc:textLeaf role="mathtex">\log(v)</sc:textLeaf> en fonction de <sc:textLeaf role="mathtex">\log(k)</sc:textLeaf>, <sc:textLeaf role="mathtex">\log[A]</sc:textLeaf> et <sc:textLeaf role="mathtex">\log [B]</sc:textLeaf>
</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\log(v)= \log(k)+ 2 \log[A]+3\log[B]</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\log(v)= \log(k)+ \log[A]^{2}+\log[B]^{3}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\log(v)= \log(k) \times 2 \log[A] \times 3\log[B]</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Le logarithme d'un produit donne la sommes de logarithme et non le produit des logarithmes.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex"> \log (a \times b) = \log ( a) + \log (b) </sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex"> \log (a^b) = b \times \log ( a) </sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\log (10^a) = a</sc:textLeaf>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
