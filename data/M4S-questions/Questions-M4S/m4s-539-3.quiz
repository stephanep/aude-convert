<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Manipuler des fractions</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF - Fraction-calcul-litt</sp:keywd>
              <sp:keywd>Contexte - maths</sp:keywd>
              <sp:keywd>Fractions</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Christophe Reillac</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est la solution de l'équation : <sc:textLeaf role="mathtex">\frac{1}{5}=\frac{1}{3}-\frac{1}{x}</sc:textLeaf>?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{1}{2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Revoir addition de fractions et inverse d'un nombre.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">2</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Revoir inverse d'un nombre.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{15}{2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{2}{15}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">C'est l'inverse</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Raisonnement </sc:inlineStyle>: On détermine d'abord <sc:textLeaf role="mathtex">\frac{1}{x}</sc:textLeaf> et on inversera ensuite l'expression.</sc:para>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Calcul</sc:inlineStyle> :</sc:para>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve">
On ajoute <sc:textLeaf role="mathtex">\frac{1}{3}</sc:textLeaf> dans chaque membre, on obtient : <sc:textLeaf role="mathtex">\frac14+\frac13=-\frac{1}{x}</sc:textLeaf></sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">Cette équation se lit dans les deux sens : <sc:textLeaf role="mathtex">-\frac{1}{x}=\frac14+\frac13</sc:textLeaf></sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">On ramène le membre de droite à une seule fraction :<sc:textLeaf role="mathtex">-\frac{1}{x}=\frac{3}{12}+\frac{4}{12}</sc:textLeaf>  <sc:textLeaf role="mathtex">=\frac{7}{12}</sc:textLeaf></sc:para>
                <sc:para xml:space="preserve">On multiplie par -1 dans chaque membre : <sc:textLeaf role="mathtex">\frac{1}{x}=-\frac{7}{12}</sc:textLeaf></sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">
On inverse l'expression : <sc:textLeaf role="mathtex">x=-\frac{12}{7}</sc:textLeaf></sc:para>
              </sc:listItem>
            </sc:itemizedList>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
