<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Trouver l'expression d'un angle ou d'une longueur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>1</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Contexte - math</sp:keywd>
              <sp:keywd>Pythagore</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Hélène Delanoe</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Le triangle ABC est rectangle en A. L'hypoténuse est le côté BC. </sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">D'après le théorème de Pythagore, quelles sont les relations correctes liant les différentes longueurs du triangle ABC entre elles ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-370-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">BC^2=AB^2+AC^2</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">BC=AB + AC</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Somme des longueurs au lieu de la somme des longueurs au carré</sc:para>
            <sc:para xml:space="preserve">Attention : <sc:textLeaf role="mathtex">\sqrt{AB^2+AC^2} \neq \sqrt{AB^2}+\sqrt{AC}^2=AB+AC</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">AB^2=BC^2+AC^2</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Mauvais repérage des côtés utilisés : c'est le carré de l'hypoténuse qui est égal au carré de la la longueur des deux autres côtés.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">AC^2=AB^2+BC^2</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Mauvais repérage des côtés utilisés : c'est le carré de l'hypoténuse qui est égal au carré de la la longueur des deux autres côtés.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">AB^2=BC^2-AC^2</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">AC=\sqrt{BC^2-AB^2}</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">D'après le théorème de Pythagore on sait que le carré de l'hypoténuse est égal à la somme des carrés des deux autres côtés.</sc:para>
            <sc:para xml:space="preserve">Ici le triangle est rectangle en A donc l'hypoténuse qui est sont côté opposé est BC.</sc:para>
            <sc:para xml:space="preserve">Ainsi on a la relation <sc:textLeaf role="mathtex">BC^2=AB^2+AC^2</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">On peut aussi l'écrire sous différentes formes telles que  <sc:textLeaf role="mathtex">AB^2=BC^2-AC^2</sc:textLeaf> <sc:textLeaf role="mathtex">\iff{}</sc:textLeaf> <sc:textLeaf role="mathtex">AC=BC^2-AB^2</sc:textLeaf> <sc:textLeaf role="mathtex">\iff{}</sc:textLeaf> <sc:textLeaf role="mathtex">AC=\sqrt{BC^2-AB^2}</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
