<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Déterminer l'expression littérale d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:level>1</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF - log - Utiliser les propriétés</sp:keywd>
              <sp:keywd>Contexte - maths</sp:keywd>
              <sp:keywd>Procédés réciproques</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">M4S - Didier Krieger</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est la formule correcte ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\ln\left(\dfrac{a}{b}\right)=\dfrac{\ln(a)}{\ln(b)}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Essayer un exemple sur votre calculatrice : <sc:textLeaf role="mathtex">\ln\left(\dfrac75\right)\approx 0,336</sc:textLeaf> et <sc:textLeaf role="mathtex">\dfrac{\ln(7)}{\ln(5)}\approx 1,209</sc:textLeaf> donc la formule est fausse</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\ln\left(\dfrac{a}{b}\right)=\ln(a)-\ln(b)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\ln\left(\dfrac{a}{b}\right)=\ln(a)+\ln(b)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Essayer un exemple sur votre calculatrice : <sc:textLeaf role="mathtex">\ln\left(\dfrac75\right)\approx 0,336</sc:textLeaf> et <sc:textLeaf role="mathtex">{\ln(7)}+{\ln(5)}\approx 3,555</sc:textLeaf> donc la formule est fausse</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\ln\left(\dfrac{a}{b}\right)=\ln(a-b)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Essayer un exemple sur votre calculatrice : <sc:textLeaf role="mathtex">\ln\left(\dfrac75\right)\approx 0,336</sc:textLeaf> et <sc:textLeaf role="mathtex">\ln(7-5)}\approx 0,693</sc:textLeaf> donc la formule est fausse</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Formule du cours à connaître : <sc:textLeaf role="mathtex">\ln\left(\dfrac{a}{b}\right)=\ln(a)-\ln(b)</sc:textLeaf></sc:para>
            <sc:para xml:space="preserve">On dit que le logarithme népérien transforme la division en soustraction</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
