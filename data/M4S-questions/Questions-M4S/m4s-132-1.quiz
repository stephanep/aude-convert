<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Projection</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
 <sc:inlineStyle role="emp">Que vaut la coordonnée <sc:textLeaf role="mathtex">V_y</sc:textLeaf> de <sc:textLeaf role="mathtex">\overrightarrow{V}</sc:textLeaf> sur l'axe des ordonnées ?</sc:inlineStyle></sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-131-1-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\cos(\alpha) * ||\overrightarrow{V}||</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Correct !</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">||\overrightarrow{V}|| / \cos(\alpha)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention de ne pas confondre la longueur du côté adjacent et celle de l'hypoténuse dans la formule.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\sin(\alpha) * ||\overrightarrow{V}||</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">C'est le côté adjacent qui nous intéresse ici, pas le côté opposé !</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex"> ||\overrightarrow{V}|| / \sin(\alpha)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention de ne pas confondre la longueur du côté opposé et de l'hypoténuse dans la formule.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Pour obtenir la coordonnée de <sc:textLeaf role="mathtex">\overrightarrow{V}</sc:textLeaf> suivant l'axe des ordonnées, il faut projeter le vecteur sur l'axe des ordonnées. Sur le schéma suivant, on retrouve un triangle ABC rectangle en C. La valeur cherchée est AC. On a <sc:textLeaf role="mathtex">\cos(\alpha) = \frac{AC}{AB}</sc:textLeaf>. Donc <sc:textLeaf role="mathtex">AC = AB * \cos(\alpha)</sc:textLeaf>. De plus, <sc:textLeaf role="mathtex">AB = ||\overrightarrow{V}||</sc:textLeaf>. Donc la projection de <sc:textLeaf role="mathtex">\overrightarrow{V}</sc:textLeaf> sur l'axe des ordonnées vaut <sc:textLeaf role="mathtex">\cos(\alpha) * ||\overrightarrow{V}||</sc:textLeaf>. L'axe Oy est orienté et <sc:textLeaf role="mathtex">V_y</sc:textLeaf> est positif.</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-132-1-1c.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
