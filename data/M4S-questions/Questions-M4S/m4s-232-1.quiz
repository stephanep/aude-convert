<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Estimer les paramètres d'un modèle</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF - fonction affine-équation droite-graphe</sp:keywd>
              <sp:keywd>Contexte - physique</sp:keywd>
              <sp:keywd>Équation caractéristique</sp:keywd>
              <sp:keywd>Pente</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Richard Dupont</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Le graphe ci-dessous représente l'évolution de la quantité de matière n du dioxygène en fonction de l'avancement de la réaction x lors de la combustion complète de 6 moles d'éthylène C<sc:textLeaf role="ind">2</sc:textLeaf>H<sc:textLeaf role="ind">4</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Cette fonction affine n de variable x a pour expression <sc:textLeaf role="mathtex">n(x) = 6 – a x</sc:textLeaf> où a est le coefficient stœchiométrique de O<sc:textLeaf role="ind">2</sc:textLeaf> dans l'équation de la réaction chimique.</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-232-1.png">
          <op:resInfoM/>
        </sp:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est la valeur de a ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">a= 1/3</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Revoir la définition de la pente (<sc:textLeaf role="mathtex">\Delta y/\Delta x</sc:textLeaf> et non l'inverse)</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">a=-3</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention la pente est -a</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">a =3</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">a=6</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">a est la pente mais n'est pas l'ordonnée à l'origine</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1567000971996">à compléter</comment></comment>-->
          <op:txt>
            <sc:para xml:space="preserve">On veut déterminer le coefficient directeur a de la droite d'expression : <sc:textLeaf role="mathtex">n(x) = 6 - a x</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">L'ordonnée à l'origine de la droite représentative de la fonction n est 6 (ordonnée du point d'intersection de la droite et de l'axe des ordonnées) et la pente est <sc:textLeaf role="mathtex">-a</sc:textLeaf> (ici négative par définition comme la droite est décroissante). On cherche une valeur de a positive.</sc:para>
            <sc:para xml:space="preserve">Il est possible de calculer la pente d'une droite grâce à la formule : <sc:textLeaf role="mathtex">-a=\frac{y_2-y_1}{x_2-x_1}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Par lecture graphique on calcule la pente comme ceci : <sc:textLeaf role="mathtex">a=\frac{6-0}{2-0}=\frac{6}{2}=3</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
