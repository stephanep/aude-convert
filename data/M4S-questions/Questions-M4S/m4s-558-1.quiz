<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Manipuler des fractions</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF -Isoler-Extraire une grandeur</sp:keywd>
              <sp:keywd>Contexte - physique</sp:keywd>
              <sp:keywd>Fractions</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Hélène Déchelette</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Pour une personne immobile, la perception de la hauteur d 'un son diffère lorsque l'émetteur du son se rapproche ou s'éloigne d'elle. La fréquence du son reçu <sc:textLeaf role="mathtex"> f_R</sc:textLeaf> est lié à la fréquence du son émis  <sc:textLeaf role="mathtex"> f_E</sc:textLeaf> par la relation : <sc:textLeaf role="mathtex">f_R = \frac{v_{son}}{v_{son} - v_E} \times f_E</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">Avec <sc:textLeaf role="mathtex">v_{son}</sc:textLeaf> = célérité du  son dans l'air= 340 m/s et <sc:textLeaf role="mathtex">v_E</sc:textLeaf> = vitesse de l'émetteur.</sc:para>
          </op:txt>
        </sp:txt>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Quelle est l'expression de la vitesse v_E de l'émetteur?</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">v_E=\frac{v_{son} (f_R-f_E)}{f_R}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">v_E=\frac{v_{son} (f_E-f_R)}{f_R}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention aux signes dans le calcul</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">v_E= \frac{f_E}{f_R}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Les termes en en <sc:textLeaf role="mathtex">v_{Son}</sc:textLeaf> ne se simplifient pas.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Raisonnement :</sc:inlineStyle> On cherche à isoler <sc:textLeaf role="mathtex">v_E</sc:textLeaf> qui est au dénominateur à droite dans l'expression : 
<sc:textLeaf role="mathtex">f_R = \frac{v_{son}}{v_{son} - v_E} \times f_E</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Calcul littéral :</sc:inlineStyle> </sc:para>
            <sc:orderedList>
              <sc:listItem>
                <sc:para xml:space="preserve">On multiplie toute l'expression par <sc:textLeaf role="mathtex">{v_{son} - v_E} </sc:textLeaf> pour faire disparaître le numérateur : 
<sc:textLeaf role="mathtex">f_R \times {(v_{son} - v_E)}= {v_{son}}{} \times f_E</sc:textLeaf></sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">On développe le membre de gauche : 
<sc:textLeaf role="mathtex">f_R v_{son} - f_R v_E= {v_{son}} f_E</sc:textLeaf></sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">On isole à gauche le terme avec <sc:textLeaf role="mathtex">v_E</sc:textLeaf> en ajoutant <sc:textLeaf role="mathtex">-f_R v_{son} </sc:textLeaf> de part et d'autre du signe égal : 
<sc:textLeaf role="mathtex"> - f_R v_E= {v_{son}} f_E - f_R v_{son}</sc:textLeaf></sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">On divise l'expression par   <sc:textLeaf role="mathtex">- f_R</sc:textLeaf> pour isoler <sc:textLeaf role="mathtex">v_E</sc:textLeaf> : 
<sc:textLeaf role="mathtex">v_E=\frac{v_{son} (f_E-f_R)}{-f_R}</sc:textLeaf>
</sc:para>
              </sc:listItem>
            </sc:orderedList>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
