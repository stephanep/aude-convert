<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3">
    <op:exeM>
      <sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Manipuler des fractions</sp:title>
      <sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#phys-mathphys-</sp:themeLicence>
      <ns0:themeLicence xmlns:ns0="http://www.utc.fr/ics/scenari/v3/primitive">#math-</ns0:themeLicence>
      <sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">2</sp:level>
      <sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L0</sp:educationLevel>
      <sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF - Fraction-calcul-litt</sp:keywd>
              <sp:keywd>Contexte - maths</sp:keywd>
              <sp:keywd>Fractions</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Hélène Kuhman</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
          <op:txt>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Quelles écritures sont égales à <sc:textLeaf role="mathtex">\frac{4x^2}{3yx}</sc:textLeaf>? ( <sc:textLeaf role="mathtex">x</sc:textLeaf> et <sc:textLeaf role="mathtex">y</sc:textLeaf> non nuls)</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{(2x)^2}{3yx}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{4x}{3y}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{(4x)^2}{3y}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">4x^2=4\times x\times x</sc:textLeaf> et <sc:textLeaf role="mathtex">(4x^2)=4x\times 4x=16x^2</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{2x\times 2x}{3x\times y}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
          <op:txt>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">On décompose le calcul en regroupant les objets de même nature, ensuite on simplifie l'expression :</sc:inlineStyle>
</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{4x^2}{3yx}=\frac{4}{3}\times \frac{x^2}{x}\times \frac{1}{y}=\frac{4x}{3y}</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
