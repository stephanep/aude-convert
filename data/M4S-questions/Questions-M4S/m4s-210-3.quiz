<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3">
    <op:exeM>
      <sp:title xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">#phys-mathphys-</sp:themeLicence>
      <ns0:themeLicence xmlns:ns0="http://www.utc.fr/ics/scenari/v3/primitive">#math-</ns0:themeLicence>
      <sp:level xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">2</sp:level>
      <sp:educationLevel xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">L0</sp:educationLevel>
      <sp:info xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Définition d'un vecteur</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
          <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1560780323751">PROBLEME_FIGURE</comment></comment>-->
          <op:txt>
            <sc:para xml:space="preserve">Dans un jeu vidéo, un personnage a huit mouvements possibles.</sc:para>
            <sc:para xml:space="preserve">1 le fait aller en haut à gauche, 2 vers le haut, 3 en haut à droite, 4 à gauche, 6 à droite, 7 en bas à gauche, 8 en bas, et 9 en bas à droite. Ces mouvements sont modélisés par les vecteurs</sc:para>
            <sc:para xml:space="preserve"><sc:textLeaf role="mathtex">\overrightarrow{V_1},\overrightarrow{V_2}, \overrightarrow{V_3}, \overrightarrow{V_4},  \overrightarrow{V_6}, \overrightarrow{V_7}, \overrightarrow{V_8}, \overrightarrow{V_9}</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" sc:refUri="images/m4s-210-3.png">
          <op:resInfoM/>
        </sp:res>
        <sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quels déplacements (représentés par des sommes de vecteurs) permet de bouger de 1 cases vers le bas et 4 cases vers la gauche ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">4 \overrightarrow{V_6} - \overrightarrow{V_2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">
Le vecteur <sc:textLeaf role="mathtex">\overrightarrow{V_6}</sc:textLeaf> permet de se déplacer d'une case vers la droite.</sc:para>
            <sc:para xml:space="preserve">Le vecteur <sc:textLeaf role="mathtex">\overrightarrow{V_2}</sc:textLeaf> permet de se déplacer d'une case vers le haut.</sc:para>
            <sc:para xml:space="preserve">Au final cette combinaison permet de se déplacer de quatre cases vers la droite et de deux cases vers le bas.</sc:para>
            <sc:para xml:space="preserve"><sc:inlineImg role="ico" sc:refUri="images/enonce-m4s-210-3-2.png"/>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">4\overrightarrow{V_7} - \overrightarrow{V_8}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">
Le vecteur <sc:textLeaf role="mathtex">\overrightarrow{V_7}</sc:textLeaf> permet de se déplacer d'une case vers le bas et d'une case vers la gauche.</sc:para>
            <sc:para xml:space="preserve">Le vecteur <sc:textLeaf role="mathtex">\overrightarrow{V_8}</sc:textLeaf> permet de se déplacer d'une case vers le bas.</sc:para>
            <sc:para xml:space="preserve">Au final cette combinaison permet de se déplacer de quatre cases vers la gauche et de trois cases vers le bas.</sc:para>
            <sc:para xml:space="preserve"><sc:inlineImg role="ico" sc:refUri="images/enonce-m4s-210-3-3.png"/>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">3\overrightarrow{V_2} -4 \overrightarrow{V_3}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:inlineImg role="ico" sc:refUri="images/enonce-m4s-210-3-4.png"/>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex"> \overrightarrow{V_8} + 3 \overrightarrow{V_6}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">
Le vecteur <sc:textLeaf role="mathtex">\overrightarrow{V_8}</sc:textLeaf> permet de se déplacer d'une case vers le bas.</sc:para>
            <sc:para xml:space="preserve">Le vecteur <sc:textLeaf role="mathtex">\overrightarrow{V_6}</sc:textLeaf> permet de se déplacer d'une case vers la droite.</sc:para>
            <sc:para xml:space="preserve">Au final cette combinaison permet de se déplacer d'une case vers le bas et de trois cases vers la droite.</sc:para>
            <sc:para xml:space="preserve"><sc:inlineImg role="ico" sc:refUri="images/enonce-m4s-210-3-5.png"/>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
          <op:txt>
            <sc:para xml:space="preserve">Pour sommer des vecteurs on peut :</sc:para>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve">Décomposer les vecteurs selon <sc:textLeaf role="mathtex">\overrightarrow{u_x}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{u_y}</sc:textLeaf> puis sommer séparément leurs composantes horizontales et leurs composantes verticales.</sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">Réaliser une somme graphique</sc:para>
              </sc:listItem>
            </sc:itemizedList>
            <sc:para xml:space="preserve">Ici la première méthode est plus rapide.</sc:para>
            <sc:para xml:space="preserve">Soustraire deux vecteurs, revient à ajouter le premier avec l'opposé du second : <sc:textLeaf role="mathtex">\overrightarrow{U} - \overrightarrow{V}</sc:textLeaf> = <sc:textLeaf role="mathtex">\overrightarrow{U} + (\overrightarrow{-V})</sc:textLeaf></sc:para>
            <sc:para xml:space="preserve">On cherche à obtenir le vecteur <sc:textLeaf role="mathtex">\overrightarrow{W}=-4\cdot \overrightarrow{u_x}-1\cdot\overrightarrow{u_y}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">On lit graphiquement : <sc:textLeaf role="mathtex">\overrightarrow{V_2}=0\cdot \overrightarrow{u_x}+1\cdot\overrightarrow{u_y}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{V_3}=1\cdot \overrightarrow{u_x}+1\cdot\overrightarrow{u_y}</sc:textLeaf></sc:para>
            <sc:para xml:space="preserve">On a donc :  <sc:textLeaf role="mathtex">3\overrightarrow{V_2}-4\overrightarrow{V_3}= -4\cdot\overrightarrow{u_x}-1\cdot\overrightarrow{u_y}=\overrightarrow{W}</sc:textLeaf></sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
