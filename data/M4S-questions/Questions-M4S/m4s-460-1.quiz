<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1567415823779">manque explication générale</comment></comment>-->
    <op:exeM>
      <sp:title>Réaliser une application numérique</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>calculette</sp:keywd>
              <sp:keywd>contexte physique</sp:keywd>
              <sp:keywd>Fonctions usuelles</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - MSedzeHoo</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Le grandissement d'une lentille entre un objet <sc:textLeaf role="mathtex">AB</sc:textLeaf> et une image <sc:textLeaf role="mathtex">A'B'</sc:textLeaf> est défini par 
<sc:textLeaf role="mathtex">\gamma=\frac {\overline{A'B'}} {\overline{AB}}</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Que vaut le grandissement lorsque <sc:textLeaf role="mathtex">\overline{AB}</sc:textLeaf> = 4cm et <sc:textLeaf role="mathtex">\overline{A'B'}</sc:textLeaf>= -8 cm ? </sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\gamma=2</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention au signe. Le grandissement est algébrique et peut donc être négatif.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\gamma=-2</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\gamma=-2cm</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Le grandissement n'a pas d'unité</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Le grandissement est défini d'après l'énoncé par l'expression : <sc:textLeaf role="mathtex">\gamma=\frac {\overline{A'B'}} {\overline{AB}}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">En remplaçant par les valeurs numériques on obtient : <sc:textLeaf role="mathtex">\gamma=\frac {-8} {4} =-2</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Par analyse dimensionnelle on a <sc:textLeaf role="mathtex">\frac{cm}{cm}</sc:textLeaf> donc le grandissement est sans unité.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
