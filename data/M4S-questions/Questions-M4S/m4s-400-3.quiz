<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Trouver l'expression d'un angle ou d'une longueur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Contexte - Chimie</sp:keywd>
              <sp:keywd>Pythagore</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Alexandre Natali</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On considère une portion de cristal de chlorure de sodium.</sc:para>
            <sc:para xml:space="preserve">Les ions sont assimilés à des sphères de rayons <sc:textLeaf role="mathtex">r_{Cl^-}</sc:textLeaf> et <sc:textLeaf role="mathtex">r_{Na^+}</sc:textLeaf>. La plus petite distance entre les ions <sc:textLeaf role="mathtex">Cl^-</sc:textLeaf> est la même que la plus petite distance entre les ions <sc:textLeaf role="mathtex">Na^+</sc:textLeaf> et est notée <sc:textLeaf role="mathtex">d_2</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Exprimer la distance <sc:textLeaf role="mathtex">d_2</sc:textLeaf> entre les ions <sc:textLeaf role="mathtex">Cl^-</sc:textLeaf>. </sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-400.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve"> <sc:textLeaf role="mathtex">d_2 = \sqrt{  (r_{Na^+}+ r_{Cl^-})^2 + (r_{Cl^-} + r _{Na^+})^2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">d_2 = \sqrt{ 2d_1^2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">d_2 = (r_{Na^+}+ r_{Cl^-})^2 + (r_{Cl^-} + r _{Na^+})^2</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">La racine carrée est oubliée.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">d_2 = d_1 \sqrt{ 2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1566376592288">Ajouter figure</comment></comment>-->
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Raisonnement :</sc:inlineStyle> On reconnaît que la distance <sc:textLeaf role="mathtex">d_2</sc:textLeaf> que l'on cherche est l'hypoténuse d'un triangle rectangle isocèle dont les petits côtés ont une longueur <sc:textLeaf role="mathtex">d_1</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">La longueur <sc:textLeaf role="mathtex">d_1</sc:textLeaf> est égale à la somme du rayon d'un atome de chlore et du rayon d'un atome de sodium : <sc:textLeaf role="mathtex">d_1=r_{Cl^-} + r _{Na^+}</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/m4s-400-3.png">
          <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1568206973516">à harmoniser avec 400-1</comment></comment>-->
          <op:resInfoM/>
        </sp:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Calcul :</sc:inlineStyle> On applique le théorème de Pythagore dans le triangle rectangle : <sc:textLeaf role="mathtex">d_2^2=d_1^2+d_1^2=2d_1^2</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Soit <sc:textLeaf role="mathtex">d_2 = \sqrt{2d_1^2}=\sqrt{2}d_1 =\sqrt{2}\times(r_{Cl^-} + r _{Na^+})</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
