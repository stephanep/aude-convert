<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Définition d'un vecteur</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Un voyageur se déplace à la vitesse <sc:textLeaf role="mathtex">||\overrightarrow{V_1}||=5 km/h</sc:textLeaf> dans un train roulant à la vitesse <sc:textLeaf role="mathtex">||\overrightarrow{V_2}||=70km/h</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">On cherche à déterminer la vitesse d'un voyageur par rapport au sol. Le vecteur vitesse associé à la vitesse du voyageur par rapport au sol est le vecteur résultant de la somme <sc:textLeaf role="mathtex">\overrightarrow{V_1}+ \overrightarrow{V_2}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
 <sc:inlineStyle role="emp">Quelle est la vitesse du voyageur par rapport au sol s'il avance dans le sens contraire que le train?</sc:inlineStyle></sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">65 km/h</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve"> Correct !</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">70km/h</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Les deux vitesses interviennent</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">5 km/h</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Les deux vitesses interviennent</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">75 km/h</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">C'est le cas où le voyageur va dans le même sens que le train</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Les vecteurs <sc:textLeaf role="mathtex">\overrightarrow{V_1}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{V_2}</sc:textLeaf> ont la même direction mais sont de sens contraires. Leurs normes se soustraient. <sc:textLeaf role="mathtex">70-5= 65</sc:textLeaf> km/h</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
