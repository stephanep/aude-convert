<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Projection</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Aude Caussarieu</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Le point M est repéré dans le plan par ses coordonnées <sc:textLeaf role="mathtex">(x,y)</sc:textLeaf>. Le vecteur <sc:textLeaf role="mathtex">\overrightarrow{OM}</sc:textLeaf> a pour norme <sc:textLeaf role="mathtex">r</sc:textLeaf> et forme un angle <sc:textLeaf role="mathtex">\theta</sc:textLeaf> avec l'axe des abscisses.</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">x=5</sc:textLeaf> cm et <sc:textLeaf role="mathtex">\theta=40</sc:textLeaf>°</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Que vaut <sc:textLeaf role="mathtex">r</sc:textLeaf> ?</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/m4S-493.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">3,4 cm</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">La norme d'un vecteur ne peut être plus petite que la norme de l'une de ses composantes.</sc:para>
            <sc:para xml:space="preserve">Faire un schéma et réécrire la relation trigonométrique entre r, x et <sc:textLeaf role="mathtex">\theta</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">7,7 cm</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Faire un schéma, construire le triangle rectangle d'hypoténuse <sc:textLeaf role="mathtex">r</sc:textLeaf> et réécrire la relation trigonométrique entre r, x et <sc:textLeaf role="mathtex">\theta</sc:textLeaf>.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">6,5 cm</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">-7,5 cm</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Une norme ne peut être négative.</sc:para>
            <sc:para xml:space="preserve">Indice : l'angle est donné en degré, et non en radians.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">6,527 cm</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Le résultat d'une application numérique ne doit pas contenir plus de chiffres significatifs que les données.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">r=\frac{x}{ \cos(\theta)}</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">Il faut faire l'application numérique en faisant attention à ce que la calculatrice soit bien en mode degrés et non radians.</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">x=\frac{5 }{\cos(40°)}=6,527</sc:textLeaf> cm</sc:para>
            <sc:para xml:space="preserve">L'arrondi se fait à 1 ou 2 chiffres significatifs (40° : 2 chiffres significatifs, 5 cm : 1 chiffre significatif)</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
