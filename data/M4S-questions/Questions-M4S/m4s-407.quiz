<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Réaliser une application numérique</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Contexte - Physique</sp:keywd>
              <sp:keywd>Conversions</sp:keywd>
              <sp:keywd>Fractions</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Alexandre Natali</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">En 1762, Franklin verse l'équivalent d'une cuillère d'huile, soit un volume V d'environ <sc:textLeaf role="mathtex">2 cm^3</sc:textLeaf>, à la surface de l'étang de Clapham en Angleterre. Une tache se forme alors à la surface et s'étend jusqu'à couvrir presque un quart de la superficie du plan d'eau, soit une surface S d'environ <sc:textLeaf role="mathtex">2000 m^2</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Quelle est l'épaisseur e de la tache ? </sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">e = 0,001 cm</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Des conversions d'unités ont été oubliées.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">e = 10^{-7} cm</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">e = 10 ^{-9} m</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">e = 1000 m</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Ce résultat n'est pas cohérent</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Application numérique</sc:inlineStyle> : On fait l'application numérique en cm : <sc:textLeaf role="mathtex">e = V / S</sc:textLeaf> avec <sc:textLeaf role="mathtex">V = 2cm^3</sc:textLeaf> et <sc:textLeaf role="mathtex">S = 2000 m^2 = 2 \cdot 10^3\cdot (10^2cm)^2</sc:textLeaf> </sc:para>
            <sc:para xml:space="preserve">On se ramène ensuite à des mètres :  <sc:textLeaf role="mathtex">e = 10^{-7}cm = 10^{-9} m</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">On notera que cette épaisseur correspond à la longueur d'une molécule d'huile.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
