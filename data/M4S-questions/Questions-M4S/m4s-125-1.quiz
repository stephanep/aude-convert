<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Déterminer l'expression littérale d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Produit scalaire</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On considère les vecteurs représentés sur le schéma.</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Quels sont les produits scalaires dont la valeur est égale à 2 ?</sc:inlineStyle></sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/m4s-1116-1 pour 125-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_5}. \overrightarrow{U_6}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">L'angle entre les deux vecteurs est obtus donc le produit scalaire est forcément négatif. </sc:para>
            <sc:para xml:space="preserve">Prouvons le par le calcul : on lit sur le graphique que <sc:textLeaf role="mathtex">\overrightarrow{U_5} \begin{pmatrix}-1\\1\end{pmatrix}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{U_1} \begin{pmatrix}1\\0\end{pmatrix}</sc:textLeaf>. On a donc : 
<sc:textLeaf role="mathtex">\overrightarrow{U_5}\cdot \overrightarrow{U_1} = (-1)\times 1 + 1\times 0 = -1</sc:textLeaf>.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_2}. \overrightarrow{U_5}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Ces deux vecteurs sont colinéaires mais de sens opposés, le produit scalaire est négatif. </sc:para>
            <sc:para xml:space="preserve">Prouvons le par le calcul. On lit sur le graphique que<sc:textLeaf role="mathtex"> \overrightarrow{U_5} \begin{pmatrix}-1\\1\end{pmatrix}</sc:textLeaf> et<sc:textLeaf role="mathtex"> \overrightarrow{U_2} \begin{pmatrix}2\\-2\end{pmatrix}</sc:textLeaf>. Donc<sc:textLeaf role="mathtex"> \overrightarrow{U_2}\cdot\overrightarrow{U_5} = 2\times 1 + (-2)\times (-1) = 4 +2 = 0</sc:textLeaf>.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_1}. \overrightarrow{U_2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_1}. \overrightarrow{U_5}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Ces deux vecteurs sont de sens opposé : leur produit scalaire est donc nécessairement négatif. </sc:para>
            <sc:para xml:space="preserve">On lit sur le graphique que<sc:textLeaf role="mathtex"> \overrightarrow{U_1} \begin{pmatrix}0\\-3\end{pmatrix}</sc:textLeaf>et<sc:textLeaf role="mathtex"> \overrightarrow{U_5} \begin{pmatrix}-1\\1\end{pmatrix}.</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_3}\cdot \overrightarrow{U_5} = 0\times (-1) + (-3) \times  1 = -3</sc:textLeaf>.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_1}. \overrightarrow{U_6}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">On cherche un produit scalaire positif :</sc:inlineStyle> il faut que les deux vecteurs impliqués soient dans le même sens. </sc:para>
            <sc:para xml:space="preserve">Parmi les propositions, c'est donc uniquement possible pour :<sc:textLeaf role="mathtex"> \overrightarrow{U_1}\cdot \overrightarrow{U_2}</sc:textLeaf>  et <sc:textLeaf role="mathtex">\overrightarrow{U_1}\cdot  \overrightarrow{U_6}</sc:textLeaf>. </sc:para>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">On calcule alors ces deux produits scalaires :</sc:inlineStyle> </sc:para>
            <sc:para xml:space="preserve">On lit sur le graphique les coordonnées suivantes</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_1} \begin{pmatrix}1\\0\end{pmatrix}</sc:textLeaf>
 ; <sc:textLeaf role="mathtex">\overrightarrow{U_2} \begin{pmatrix}2\\-2\end{pmatrix}</sc:textLeaf> ; 
<sc:textLeaf role="mathtex">\overrightarrow{U_6} \begin{pmatrix}2\\0\end{pmatrix}</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">Comme nous sommes dans un repère orthonormé on en déduit :</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_1}. \overrightarrow{U_2} = 2\times 1 + (-2)\times 0 = 2</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{U_1}. \overrightarrow{U_6} = 1\times 2 + 0\times 0 = 2</sc:textLeaf>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
