<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Trouver l'expression d'un angle ou d'une longueur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>4</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Contexte - Physique</sp:keywd>
              <sp:keywd>Thales</sp:keywd>
              <sp:keywd>Trigonometrie</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Hélène Delanoe</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Une personne observe la tour Eiffel de sa fenêtre. En tenant une règle à bout de bras, elle mesure une hauteur apparente de 30 cm. Les bras de cette personne mesurent environ 1 mètre de longueur et la tour Eiffel mesure elle 300 mètres de hauteur. On cherche à déterminer la distance qui sépare cette personne de la tour Eiffel.</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quel outil va-t-on utiliser pour résoudre ce problème ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">Le théorème de Thalès</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">Les formules de trigonométrie sur les tangentes.</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">Les formules de trigonométrie sur les sinus</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Il est impossible d'appliquer les formules de trigonométries sur les sinus car nous n'avons aucune information sur la longueur de l'hypoténuse.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">Les formules de trigonométrie sur les cosinus.</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Il est impossible d'appliquer les formules de trigonométries sur les cosinus car nous n'avons aucune information sur la longueur de l'hypoténuse.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">Le théorème de Pythagore.</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Il est impossible d'appliquer le théorème de Pythagore car nous n'avons aucune information sur la longueur de l'hypoténuse.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Raisonnement </sc:inlineStyle>: Il est complètement équivalent d'appliquer le théorème de Thalès ou d'utiliser les formules trigonométriques sur les tangentes.</sc:para>
            <sc:para xml:space="preserve">D'après l'énoncé et la formule ci-contre on sait que <sc:textLeaf role="mathtex">AB=300m</sc:textLeaf>,  <sc:textLeaf role="mathtex">A'B'=30cm</sc:textLeaf> et <sc:textLeaf role="mathtex">B'C=1m</sc:textLeaf> et on cherche la distance à la tour Eiffel c'est-à-dire <sc:textLeaf role="mathtex">BC</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Démonstration avec le théorème de Thalès :</sc:inlineStyle>
            </sc:para>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve">Le théorème de Thalès nous donne la formule suivant : <sc:textLeaf role="mathtex">\frac{A'B'}{AB}=\frac{B'C}{BC}=\frac{A'C}{AC}</sc:textLeaf>. </sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">Comme les valeurs de AB, A'B' et B'C sont connues on a l'égalité : <sc:textLeaf role="mathtex">BC=\frac{B'C\times{AB}}{A'B'}=\frac{1\times{300}}{30}\times\frac{1}{10^{-2}}\times\frac{m\times{m}}{m}=1000 m=1 km</sc:textLeaf>.</sc:para>
              </sc:listItem>
            </sc:itemizedList>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Démonstration avec  les formules de trigonométrie sur les tangentes </sc:inlineStyle>
            </sc:para>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve">Soit <sc:textLeaf role="mathtex">\alpha</sc:textLeaf> l'angle <sc:textLeaf role="mathtex">\widehat{ACB}</sc:textLeaf>. D'après les règles de trigonométries appliquées aux tangentes on a <sc:textLeaf role="mathtex">\tan{\alpha}=\frac{AB}{BC}</sc:textLeaf> mais aussi <sc:textLeaf role="mathtex">\tan{\alpha}=\frac{A'B'}{B'C}</sc:textLeaf>. </sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">Ainsi on obtient l'égalité <sc:textLeaf role="mathtex">\frac{AB}{BC}=\frac{A'B'}{B'C}</sc:textLeaf> <sc:textLeaf role="mathtex">\iff{}</sc:textLeaf> <sc:textLeaf role="mathtex">BC=\frac{B'C \times AB}{A'B'} =\frac{1\times{300}}{30}\times\frac{1}{10^{-2}}\times\frac{m\times{m}}{m}=1000 \mbox{ m}=1 \mbox{ km}</sc:textLeaf>.</sc:para>
              </sc:listItem>
            </sc:itemizedList>
            <sc:para xml:space="preserve">On obtient donc le même résultat quelle que soit la méthode utilisée.</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/m4s-387.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
