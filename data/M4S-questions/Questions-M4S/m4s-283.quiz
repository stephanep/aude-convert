<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>2</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Vecteurs caractéristiques coordonnées</sp:keywd>
              <sp:keywd>contexte physique</sp:keywd>
              <sp:keywd>Définition d'un vecteur</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - H. Dechelette</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Une planète A dont le centre de gravité est au point <sc:textLeaf role="mathtex">O_A</sc:textLeaf> exerce une force d'attraction gravitationnelle sur une planète B dont le centre de gravité est au point  <sc:textLeaf role="mathtex">O_B</sc:textLeaf> . cette force est représentée sur le schéma ci dessous :</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Quelles sont les formes d'écriture que l'on peut utiliser pour noter ce vecteur force ?</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/m4s-283.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\vec F_{A/B}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\vec F_{AsurB}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\vec F_{B/A}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Il faut faire attention à l'objet qui agit et à celui qui subit l'action.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow {AB}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Cette proposition de vecteur n'est pas un vecteur force. Il ne s'exprime d'ailleurs pas en Newton.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{BA}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Cette proposition de vecteur n'est pas un vecteur force. Il ne s'exprime d'ailleurs pas en Newton.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{O_BO_A}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Il ne faut pas confondre le vecteur force et le bipoint correspondant aux centres des 2 objets étudiés.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow {O_AO_B}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Il ne faut pas confondre le vecteur force et le bipoint correspondant aux centres des 2 objets étudiés.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">À savoir </sc:inlineStyle>: Il existe deux types de vecteurs.</sc:para>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve">Des vecteurs qui modélisent <sc:inlineStyle role="emp">un déplacement</sc:inlineStyle> et qui sont notés avec deux lettres (point de départ et point d'arrivée) comme le vecteur <sc:textLeaf role="mathtex">\overrightarrow{OM}</sc:textLeaf> ou le vecteur <sc:textLeaf role="mathtex">\overrightarrow{AB}</sc:textLeaf>. La norme de ces vecteurs est homogène à une distance (unité = mètre ou km ou ...)</sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">Des vecteurs qui ne modélisent pas un déplacement mais qui modélisent une <sc:inlineStyle role="emp">grandeur physique</sc:inlineStyle>. Leur norme n'est pas homogène à une distance. C'est le cas par exemple de la force (norme en Newton) ou de la vitesse (norme en m/s). Ces vecteurs sont notés avec une lettre majuscule et parfois du texte en indice. Ex : <sc:textLeaf role="mathtex">\overrightarrow{F_{A/B}}</sc:textLeaf> ;  <sc:textLeaf role="mathtex">\overrightarrow{V}}</sc:textLeaf> ;  <sc:textLeaf role="mathtex">\overrightarrow{E_{1}}</sc:textLeaf> </sc:para>
              </sc:listItem>
            </sc:itemizedList>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
