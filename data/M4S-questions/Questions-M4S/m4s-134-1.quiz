<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Estimer les paramètres d'un modèle</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>2</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF - fonction affine-équation droite-graphe</sp:keywd>
              <sp:keywd>Contexte - physique</sp:keywd>
              <sp:keywd>Équation caractéristique</sp:keywd>
              <sp:keywd>Pente</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Richard Dupont</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">La caractéristique d'un générateur a pour expression U = E - r × I</sc:para>
            <sc:para xml:space="preserve">U est la tension (unité : volt V) aux bornes du générateur, I est l'intensité (unité : ampère A) qui le traverse, E est la force électromotrice du générateur (unité : volt V) et r sa résistance interne (unité : ohm Ω).</sc:para>
            <sc:para xml:space="preserve">On a tracé les caractéristiques de cinq générateurs différents (E<sc:textLeaf role="ind">1</sc:textLeaf>, r<sc:textLeaf role="ind">1</sc:textLeaf>), (E<sc:textLeaf role="ind">2</sc:textLeaf>, r<sc:textLeaf role="ind">2</sc:textLeaf>), (E<sc:textLeaf role="ind">3</sc:textLeaf>, r<sc:textLeaf role="ind">3</sc:textLeaf>), (E<sc:textLeaf role="ind">4</sc:textLeaf>, r<sc:textLeaf role="ind">4</sc:textLeaf>) et (E<sc:textLeaf role="ind">5</sc:textLeaf>, r<sc:textLeaf role="ind">5</sc:textLeaf>).</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-134-1.png">
          <op:resInfoM/>
        </sp:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelles sont les affirmations exactes parmi les propositions suivantes?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">La force électromotrice du générateur 3 : E<sc:textLeaf role="ind">3</sc:textLeaf> est identique à la force électromotrice du générateur 4 : E<sc:textLeaf role="ind">4</sc:textLeaf>.</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">La force électromotrice du générateur 1 : E<sc:textLeaf role="ind">1</sc:textLeaf> est supérieure à la force électromotrice du générateur 2 : E<sc:textLeaf role="ind">2</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">La résistance interne du générateur 5 : r<sc:textLeaf role="ind">5</sc:textLeaf> est inférieure à la résistance interne de l'électrolyseur 4 : r<sc:textLeaf role="ind">4</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Il faut comparer les pentes (-r) des deux droites</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">La résistance interne du générateur 3 : r<sc:textLeaf role="ind">3</sc:textLeaf> est égale à la résistance interne du générateur 1 : r<sc:textLeaf role="ind">1</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">Le générateur 1 possède la plus petite résistance interne</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Il faut rechercher la droite dont la pente la plus petite (en valeur absolue) ?</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">il faut associer E à l'ordonnée à l'origine et r à la pente (ici négative par définition)</sc:para>
            <sc:para xml:space="preserve">dans ce cas, les résistances et les pentes des droites sont de signe contraires.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
