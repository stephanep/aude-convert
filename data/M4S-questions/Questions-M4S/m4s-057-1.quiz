<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Trouver l'expression d'un angle ou d'une longueur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Mathématiques</sp:keywd>
              <sp:keywd>Trigonométrie</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - H. Delanoë-Ayari</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Dans la figure ci-dessous la distance <sc:textLeaf role="mathtex">DI</sc:textLeaf> vaut <sc:textLeaf role="mathtex">a</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
 <sc:inlineStyle role="emp">Parmi les égalités suivantes, lesquelles sont correctes?</sc:inlineStyle></sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/m4s-57-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">DJ=a\times{\cos(\alpha)}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\tan(\alpha)=\frac{DK}{DJ}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">DK=a/\sin(\alpha)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Rappel : <sc:textLeaf role="mathtex">\sin(\alpha)=\frac{\text{côté opposé}}{\text{hypoténuse}}</sc:textLeaf></sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">DK=a\times{\sin(\alpha)}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">DJ=a\times{\sin(\alpha)}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Rappel : <sc:textLeaf role="mathtex">\sin(\alpha)=\frac{\text{côté opposé}}{\text{hypoténuse}}</sc:textLeaf></sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Sachant que la somme des angles d'un rectangle vaut 180° (ce sont des angles supplémentaires) et en appliquant cette propriété dans le triangle rectangle AFD on en déduit que l'angle <sc:textLeaf role="mathtex">\widehat{ADF}=90-\alpha °</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Or le triangle ADE est également rectangle en D donc on en déduit que <sc:textLeaf role="mathtex">\widehat{IDJ}=\widehat{BAC}=\alpha</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">On utilise ensuite les relations de trigonométrie suivantes : <sc:textLeaf role="mathtex">\cos(\alpha)=\frac{\text{côté adjacent}}{\text{hypoténuse}}</sc:textLeaf>, <sc:textLeaf role="mathtex">\sin(\alpha)=\frac{\text{côté opposé}}{\text{hypoténuse}}</sc:textLeaf> et <sc:textLeaf role="mathtex">\tan(\alpha)=\frac{\text{côté opposé}}{\text{côté adjacent}}</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
