<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>4</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>connaitre</sp:keywd>
              <sp:keywd>contexte physique</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - MSedzeHoo</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">L'énergie fournie par une chaudières à méthane pendant une année vaut <sc:textLeaf role="mathtex">1,84\times10^6 </sc:textLeaf> kW.h.</sc:para>
            <sc:para xml:space="preserve">Le pouvoir calorifique du méthane est égal à l'énergie qu'il libère lors de sa combustion avec le dioxygène dans la chaudière et vaut <sc:textLeaf role="mathtex">P_C=802,27kJ.mol^{-1}</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Quelle est la quantité de matière de méthane <sc:textLeaf role="mathtex">n</sc:textLeaf> (en <sc:textLeaf role="mathtex">mol</sc:textLeaf>) consommée en une année ?</sc:inlineStyle></sc:para>
            <sc:para xml:space="preserve">Donnée : <sc:textLeaf role="mathtex">1kW.h=3,6\times10^3kJ</sc:textLeaf>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">n=1,2\times10^{-7}mol</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Erreur lors du calcul de <sc:textLeaf role="mathtex">n</sc:textLeaf>:<sc:textLeaf role="mathtex">n\ne{\frac{P_C}{Q}}</sc:textLeaf> qui donne comme unité des <sc:textLeaf role="mathtex">\frac{kJ.mol^{-1}}{kJ}=mol^{-1}=\frac{1}{mol}\ne{mol}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">n=8,3\times10^6mol</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">n=5,3\times10^{12}mol</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Erreur lors du calcul de <sc:textLeaf role="mathtex">n</sc:textLeaf>:<sc:textLeaf role="mathtex">n\ne{{P_C}\times{Q}}</sc:textLeaf> qui donne comme unité des <sc:textLeaf role="mathtex">kJ.mol^{-1}\times{kJ}=kJ^2mol^{-1}\ne{mol}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">n=0,64mol</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">
Erreur lors de la conversion des kW.h en kJ : <sc:textLeaf role="mathtex">n=\frac{Q}{P_C}=\frac{1,84\times10^6}{3,6\times10^3\times802,27}\times\frac{kJ}{\frac{kJ}{mol}}=0,64mol</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Modélisation</sc:inlineStyle> : L'énergie totale libérée par la combustion dans la chaudière <sc:textLeaf role="mathtex">Q\textsubscript{ch}</sc:textLeaf> est égale au produit du pouvoir calorifique du méthane <sc:textLeaf role="mathtex">P_C</sc:textLeaf> par le nombre de moles de méthane consommées <sc:textLeaf role="mathtex">n</sc:textLeaf> : <sc:textLeaf role="mathtex">Q\textsubscript{ch}=P_c\cdot n</sc:textLeaf>. </sc:para>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Raisonnement</sc:inlineStyle> : on cherche <sc:textLeaf role="mathtex">n</sc:textLeaf> dans l'expression  <sc:textLeaf role="mathtex">Q\textsubscript{ch}=P_c\cdot n</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Calcul littéral </sc:inlineStyle>: On isole <sc:textLeaf role="mathtex">n</sc:textLeaf> en divisant par son préfacteur : <sc:textLeaf role="mathtex">n=\frac{Q_ch}{P_c}</sc:textLeaf></sc:para>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Application numérique</sc:inlineStyle> : <sc:textLeaf role="mathtex">n=\frac{1,84\times10^6\times3,6\times10^3\mbox{ kJ}}{802,27\mbox{ kJ/mol}}</sc:textLeaf></sc:para>
            <sc:para xml:space="preserve">Comme 
<sc:textLeaf role="mathtex">\frac{kJ}{\frac{kJ}{mol}}={kJ}\times{\frac{mol}{kJ}}=mol</sc:textLeaf>, on obtient : <sc:textLeaf role="mathtex">n = 8,3 \times10^6mol</sc:textLeaf> </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
