<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Déterminer l'expression littérale d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>1</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Produit scalaire</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Comment s'exprime le produit scalaire entre <sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> et<sc:textLeaf role="mathtex"> \overrightarrow{v}</sc:textLeaf> ? <sc:textLeaf role="mathtex">( \overrightarrow{u}, \overrightarrow{v})</sc:textLeaf> représente l'angle non-orienté entre <sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{v}</sc:textLeaf>?</sc:inlineStyle></sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">||\overrightarrow{u}|| .||\overrightarrow{v} || \cos(\overrightarrow{u}, \overrightarrow{v})</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">||\overrightarrow{u}|| .||\overrightarrow{v} || \sin(\overrightarrow{u}, \overrightarrow{v})</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">C'est le <sc:textLeaf role="mathtex">\cos</sc:textLeaf> qui intervient ici</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">||\overrightarrow{u}||. ||\overrightarrow{v} || \cos( \overrightarrow{v}, \overrightarrow{u})</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">||\overrightarrow{u}|| .||\overrightarrow{v} || \sin( \overrightarrow{v}, \overrightarrow{u})</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">C'est le <sc:textLeaf role="mathtex">\cos</sc:textLeaf> qui intervient ici</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Voir sur unisciel http://uel.unisciel.fr/physique/outils_nancy/outils_nancy_ch03/co/apprendre_04_01.html</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
