<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Projection</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Aude Caussarieu</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Le point M est repéré dans le plan par ses coordonnées <sc:textLeaf role="mathtex">(x,y)</sc:textLeaf>. Le vecteur <sc:textLeaf role="mathtex">\overrightarrow{OM}</sc:textLeaf> a pour norme <sc:textLeaf role="mathtex">r</sc:textLeaf> et forme un angle <sc:textLeaf role="mathtex">\theta</sc:textLeaf> avec l'axe des abscisses.</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">x=3</sc:textLeaf> cm et <sc:textLeaf role="mathtex">y=4</sc:textLeaf> cm</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Que vaut <sc:textLeaf role="mathtex">r</sc:textLeaf> ?</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/m4S-493.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">7 cm</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Faire un schéma : la longueur de l'hypoténuse d'un triangle rectangle (<sc:textLeaf role="mathtex">r</sc:textLeaf>) ne peut être égale à la somme des longueurs de ses côtés. Sinon le triangle est plat !</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">2,6 cm</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Indice : Appliquer le théorème de Pythagore et ne pas oublier les carrés</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">5 cm</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Le théorème de Pythagore appliqué dans le triangle rectangle dont l'hypoténuse est OM, et dont les deux autres côtés ont pour longueur x et y.</sc:para>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">r^2=x^2+y^2 \rightarrow r=\sqrt{x^2+y^2}</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">Application numérique : <sc:textLeaf role="mathtex">r=\sqrt{3^2+4^2}=\sqrt{25}=5 cm</sc:textLeaf>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
