<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Déterminer l'expression littérale d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Système d'équations</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On désigne par  <sc:textLeaf role="mathtex">\begin{pmatrix}x\\y\end{pmatrix}</sc:textLeaf> et <sc:textLeaf role="mathtex">\begin{pmatrix}x'\\y'\end{pmatrix}</sc:textLeaf>  les coordonnées dans la base <sc:textLeaf role="mathtex">(\overrightarrow{i}, \overrightarrow{j})</sc:textLeaf> de deux vecteurs <sc:textLeaf role="mathtex">\overrightarrow{u}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{v}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelles relations doivent vérifier les nombres réels <sc:textLeaf role="mathtex">x,y,x',y'</sc:textLeaf> pour que <sc:textLeaf role="mathtex">\overrightarrow{u}-2\overrightarrow{v}=\overrightarrow{0}</sc:textLeaf>?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">x=2x'</sc:textLeaf> et <sc:textLeaf role="mathtex">y=2y'</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">2x-x'=0</sc:textLeaf> et <sc:textLeaf role="mathtex">2y-y'=0</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Ici <sc:textLeaf role="mathtex">2\overrightarrow{u}-\overrightarrow{v}=\overrightarrow{0}</sc:textLeaf>.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">x.x'=0</sc:textLeaf> et <sc:textLeaf role="mathtex">y.y'=0</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">x=-2x'</sc:textLeaf> et <sc:textLeaf role="mathtex">y=-2y'</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Ici <sc:textLeaf role="mathtex">\overrightarrow{u}+2\overrightarrow{v}=\overrightarrow{0}</sc:textLeaf> soit <sc:textLeaf role="mathtex">\overrightarrow{u}=-2\overrightarrow{v}</sc:textLeaf>.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">2x+x'=0</sc:textLeaf> et <sc:textLeaf role="mathtex">2y+y'=0</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Ici <sc:textLeaf role="mathtex">2\overrightarrow{u}+2\overrightarrow{v}=\overrightarrow{0}</sc:textLeaf>.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{u}-2\overrightarrow{v}=\overrightarrow{0}</sc:textLeaf> si toutes les coordonnées du vecteurs <sc:textLeaf role="mathtex">\overrightarrow{u}-2\overrightarrow{v}</sc:textLeaf> sont nulles c'est-à-dire <sc:textLeaf role="mathtex">x-2x'=0</sc:textLeaf> et <sc:textLeaf role="mathtex">y-2y'=0</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
