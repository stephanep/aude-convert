<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>2</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Vecteurs caractéristiques coordonnées</sp:keywd>
              <sp:keywd>contexte physique</sp:keywd>
              <sp:keywd>niveau 1</sp:keywd>
              <sp:keywd>Définition d'un vecteur</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - H. Dechelette</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">La force électrostatique <sc:textLeaf role="mathtex"> \vec{F}</sc:textLeaf> qui s'exerce sur une particule de charge positive q placée dans un champ  <sc:textLeaf role="mathtex"> \vec{E}</sc:textLeaf> vérifie la relation :<sc:textLeaf role="mathtex"> \vec{E} ={ \vec{F}\over q}</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Quelle est l'affirmation exacte sur <sc:textLeaf role="mathtex">\vec{F}</sc:textLeaf>  et   <sc:textLeaf role="mathtex"> \vec{E}</sc:textLeaf>  ?</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve"> <sc:textLeaf role="mathtex"> \vec{E}</sc:textLeaf>
et   <sc:textLeaf role="mathtex"> \vec{F}</sc:textLeaf> sont de <sc:inlineStyle role="emp">sens identique</sc:inlineStyle>, de <sc:inlineStyle role="emp">direction identique</sc:inlineStyle> mais de <sc:inlineStyle role="emp">norme différente</sc:inlineStyle>.</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve"><sc:textLeaf role="mathtex"> \vec{E}</sc:textLeaf>
et   <sc:textLeaf role="mathtex"> \vec{F}</sc:textLeaf> sont de <sc:inlineStyle role="emp">sens identique</sc:inlineStyle>, de <sc:inlineStyle role="emp">direction identique</sc:inlineStyle> et de <sc:inlineStyle role="emp">norme identique</sc:inlineStyle>.</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">La valeur de la force et du champs électrique sont différents du fait de la présence de la charge "q"  dans la formule.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve"><sc:textLeaf role="mathtex"> \vec{E}</sc:textLeaf>
et   <sc:textLeaf role="mathtex"> \vec{F}</sc:textLeaf> sont de <sc:inlineStyle role="emp">sens différent</sc:inlineStyle>, de <sc:inlineStyle role="emp">direction identique</sc:inlineStyle> et de <sc:inlineStyle role="emp">norme différente</sc:inlineStyle>.</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Comme la charge ici est une grandeur positive, les 2 vecteurs ont même sens.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve"><sc:textLeaf role="mathtex"> \vec{E}</sc:textLeaf>
et   <sc:textLeaf role="mathtex"> \vec{F}</sc:textLeaf> sont de <sc:inlineStyle role="emp">sens différent</sc:inlineStyle>, de <sc:inlineStyle role="emp">direction différente</sc:inlineStyle> et de <sc:inlineStyle role="emp">norme différente</sc:inlineStyle>.</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Les 2 vecteurs sont verticaux, il sont donc la même direction. Comme la charge est une grandeur positive, les 2 vecteurs ont même sens.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Comme la charge de la particule est ici  une grandeur positive, les vecteurs force et champ électrostatique ont même direction, même sens .</sc:para>
            <sc:para xml:space="preserve">Seules leurs normes diffèrent.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
