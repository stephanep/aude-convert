<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Estimer les paramètres d'un modèle</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF - fonction affine-équation droite-graphe</sp:keywd>
              <sp:keywd>Contexte - physique</sp:keywd>
              <sp:keywd>Équation caractéristique</sp:keywd>
              <sp:keywd>Pente</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Richard Dupont</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Lors d'une expérience de chimie du dosage de l'acide méthanoïque par la soude, on se réfère à la Loi de Henderson : <sc:textLeaf role="mathtex"> \mbox{pH} = \mbox{pK}_A + \log \left(\frac{[HCO_2^-]}{[HCOOH]} \right)</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">On a ainsi tracé la courbe représentative de la fonction <sc:textLeaf role="mathtex">f </sc:textLeaf>: <sc:textLeaf role="mathtex">\mbox{pH} = f \left( \log\left( \frac{[HCO_2^-]}{[HCOOH]} \right)\right)</sc:textLeaf></sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-471-1.png">
          <op:resInfoM/>
        </sp:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On déduit de cette représentation le <sc:textLeaf role="mathtex">pK_A</sc:textLeaf> du couple acide méthanoïque-ion méthanoate.</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est la valeur du <sc:textLeaf role="mathtex">\mbox{pK}_A</sc:textLeaf>?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\mbox{pK}_A = 1</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Confusion avec la pente</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\mbox{pK}_A = 3,8</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\mbox{pK}_A = 1,8</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">L'ordonnée à l'origine est l'ordonnée du point de la droite a pour abscisse 0 et ce point n'est pas situé à l’extrême gauche du graphique.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\mbox{pK}_A = -2</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">L'ordonnée à l'origine  n'est pas l’abscisse du point situé à l'extrême gauche du graphique.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Raisonnement :</sc:inlineStyle> En mathématiques l'expression typique d'une droite est <sc:textLeaf role="mathtex">y=ax+b</sc:textLeaf> avec <sc:textLeaf role="mathtex">a</sc:textLeaf> le coefficient directeur (pente) et <sc:textLeaf role="mathtex">b</sc:textLeaf> l'ordonnée à l'origine.</sc:para>
            <sc:para xml:space="preserve">Dans la situation de chimie présentée ici :</sc:para>
            <sc:table role="">
              <sc:column width="50"/>
              <sc:column width="50"/>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Situation de chimie présentée ici</sc:para>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">\mbox{pH} = \mbox{pK}_A + \log \left(\frac{[HCO_2^-]}{[HCOOH]} \right)</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">Situation typique de mathématiques</sc:para>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">y=b+a\cdot x</sc:textLeaf>
                  </sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">\mbox{pH}</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">y</sc:textLeaf>
                  </sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">\log\left( \frac{[HCO_2^-]}{[HCOOH]} \right)</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve"><sc:textLeaf role="mathtex">x</sc:textLeaf> : variable</sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">1</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve"><sc:textLeaf role="mathtex">a</sc:textLeaf> : pente</sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">\mbox{pK}_A</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve"><sc:textLeaf role="mathtex">b</sc:textLeaf> : ordonnée à l'origine</sc:para>
                </sc:cell>
              </sc:row>
            </sc:table>
            <sc:para xml:space="preserve">Chercher la valeur du pk<sc:textLeaf role="ind">A</sc:textLeaf> revient donc à chercher l'ordonnée à l'origine de la droite représentée.</sc:para>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Lecture graphique </sc:inlineStyle>: La droite coupe l'axe des ordonnées en <sc:textLeaf role="mathtex">\mbox{pH}=3,8</sc:textLeaf>. On a donc  <sc:textLeaf role="mathtex">\mbox{pK}_A=3,8</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
