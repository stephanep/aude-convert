<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Trouver l'expression d'un angle ou d'une longueur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Contexte - Chimie</sp:keywd>
              <sp:keywd>Pythagore</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Alexandre Natali</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On considère une portion de cristal de chlorure de sodium. Les ions sont assimilés à des sphères de rayons <sc:textLeaf role="mathtex">r_{Cl^-}= 181</sc:textLeaf> pm et <sc:textLeaf role="mathtex">r_{Na^+}=97</sc:textLeaf> pm.</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Que vaut <sc:textLeaf role="mathtex">d_3</sc:textLeaf> ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-400.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">386420 pm</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Racine carrée oubliée dans le calcul.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">622 pm</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">29 pm</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Les grandeurs sous la racine n'ont pas été portées au carré.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1566376891435">Ajouter une figure</comment></comment>-->
      <op:res>
        <sp:txt>
          <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1568206985397">À harmoniser avec 400-1</comment></comment>-->
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Raisonnement :</sc:inlineStyle> On reconnaît que la distance <sc:textLeaf role="mathtex">d_3</sc:textLeaf> que l'on cherche est l'hypoténuse d'un triangle rectangle dont le plus petit côté a une longueur <sc:textLeaf role="mathtex">d_1</sc:textLeaf> sachant que la longueur <sc:textLeaf role="mathtex">d_1</sc:textLeaf> est égale à la somme du rayon d'un atome de chlore et du rayon d'un atome de sodium : <sc:textLeaf role="mathtex">d_1=r_{Cl^-} + r _{Na^+}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">L'autre côté du triangle rectangle est égal à la somme de deux rayons d'atome de chlore et de deux rayons d'atome de sodium. Il vaut donc : <sc:textLeaf role="mathtex">2d_1</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/m4s-400-4.png">
          <op:resInfoM/>
        </sp:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Calcul :</sc:inlineStyle> On applique le théorème de Pythagore dans le triangle rectangle : <sc:textLeaf role="mathtex">d_3^2=d_1^2+(2d_1)^2=d_1^2+4d_1^2=5d_1^2</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Soit <sc:textLeaf role="mathtex">d_3 = \sqrt{5d_1^2}=\sqrt{5\times(r_{Cl^-} + r _{Na^+})^2}=\sqrt{5}\times(r_{Cl^-} + r _{Na^+})</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Application numérique </sc:inlineStyle>: <sc:textLeaf role="mathtex">d_3= \sqrt{5}\times (181 \mbox{ pm}+ 97\mbox{ pm})\approx 622 \mbox{ pm}</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
