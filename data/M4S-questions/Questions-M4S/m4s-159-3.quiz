<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Projection</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Soit le repère <sc:textLeaf role="mathtex">(O,x,y)</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Un mobile glisse sans frottement le long d'un plan incliné faisant un angle <sc:textLeaf role="mathtex">\alpha</sc:textLeaf> avec un plan horizontal. Il est soumis à son poids vertical <sc:textLeaf role="mathtex">\overrightarrow{P}</sc:textLeaf> et à une réaction du support <sc:textLeaf role="mathtex">\overrightarrow{R}</sc:textLeaf>. On notera <sc:textLeaf role="mathtex">||\overrightarrow{R}||</sc:textLeaf> la norme de la réaction du support.</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">
Quelles sont les expressions des composantes <sc:textLeaf role="mathtex">R_x</sc:textLeaf> et <sc:textLeaf role="mathtex">R_y</sc:textLeaf> de la réaction du support dans le repère <sc:textLeaf role="mathtex">(O,x,y)</sc:textLeaf>?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-159-1-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">R_x= ||\overrightarrow{R}|| </sc:textLeaf> et <sc:textLeaf role="mathtex">R_y= 0</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Aide : Indiquer sur le schéma les grandeurs recherchées</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">R_x= ||\overrightarrow{R}|| * \sin(\alpha)</sc:textLeaf> et <sc:textLeaf role="mathtex">R_y= ||\overrightarrow{R}|| * \cos(\alpha)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention au repère choisi</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">R_x= 0</sc:textLeaf> et <sc:textLeaf role="mathtex">R_y= ||\overrightarrow{R}||</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Correct !</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">R_x= 0</sc:textLeaf> et <sc:textLeaf role="mathtex">R_y= - ||\overrightarrow{R}|| </sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention à l'orientation des axes.</sc:para>
            <sc:para xml:space="preserve">Aide : Indiquer sur le schéma les grandeurs recherchées</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">R_x=  ||\overrightarrow{R}|| * \cos(\alpha)</sc:textLeaf> et <sc:textLeaf role="mathtex">R_y=  ||\overrightarrow{R}|| * \sin(\alpha)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention au repère choisi</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">

On constate que <sc:textLeaf role="mathtex">R_x =0</sc:textLeaf> et <sc:textLeaf role="mathtex">R_y&gt;0</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">D'où : <sc:textLeaf role="mathtex">R_x= 0</sc:textLeaf> et <sc:textLeaf role="mathtex">R_y=  ||\overrightarrow{R}|| </sc:textLeaf>
</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-159-3-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
