<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Réaliser une application numérique</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Log Fonctions</sp:keywd>
              <sp:keywd>Contexte physique</sp:keywd>
              <sp:keywd>Fonctions usuelles</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - H. Dechelette</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">En physique, l'intensité sonore <sc:textLeaf role="mathtex">I</sc:textLeaf> et le niveau sonore <sc:textLeaf role="mathtex">L</sc:textLeaf> sont liés par  la relation <sc:textLeaf role="mathtex">L = 10 \times \log \left({I \over I_0}\right)</sc:textLeaf> avec <sc:textLeaf role="mathtex">I_0 = 10^{-12} W/m^{2}</sc:textLeaf> et <sc:textLeaf role="mathtex">L</sc:textLeaf> en décibel (dB). </sc:para>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve">Une perceuse émet un son de niveau sonore <sc:textLeaf role="mathtex"> L_ 1</sc:textLeaf> grâce à une intensité sonore de <sc:textLeaf role="mathtex"> I_1 = 10^{-4} \ \mbox{W}.\mbox{m}^{-2}</sc:textLeaf>
</sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve"><sc:textLeaf role="mathtex">n</sc:textLeaf> perceuses émettent un son de niveau sonore <sc:textLeaf role="mathtex"> L_ n</sc:textLeaf> grâce à une intensité sonore <sc:textLeaf role="mathtex"> I_n = n \times I_1</sc:textLeaf>
</sc:para>
              </sc:listItem>
            </sc:itemizedList>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Quel est le tableau qui donne les bonnes valeurs du niveau sonore en fonction de l'intensité sonore ?</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:table role="">
              <sc:column width="23"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Nombre de perceuses</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">1</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">2</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">3</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">4</sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Niveau sonore <sc:textLeaf role="mathtex">L</sc:textLeaf> (dB)</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">80,0</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">83,0</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">84,7</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">86,0</sc:para>
                </sc:cell>
              </sc:row>
            </sc:table>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Bonne réponse</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:table role="">
              <sc:column width="23"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Nombre de perceuses</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">1</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">2</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">3</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">4</sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Niveau sonore <sc:textLeaf role="mathtex">L</sc:textLeaf> (dB)</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">80,0</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">160</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">240</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">320</sc:para>
                </sc:cell>
              </sc:row>
            </sc:table>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention, il n'y a pas proportionnalité entre le nombre de perceuses et le niveau sonore <sc:textLeaf role="mathtex">L</sc:textLeaf></sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:table role="">
              <sc:column width="23"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Nombre de perceuses</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">1</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">2</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">3</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">4</sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Niveau sonore <sc:textLeaf role="mathtex">L</sc:textLeaf> (dB)</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">80,0</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">83,0</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">86,0</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">89,0</sc:para>
                </sc:cell>
              </sc:row>
            </sc:table>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">La règle qui donne 3 dB de plus si on "DOUBLE" le nombre de sources n'est pas applicable au passage de 3 à 4 perceuses car on ne double pas le nombre de perceuses.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:table role="">
              <sc:column width="23"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:column width="5"/>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Nombre de perceuses</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">1</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">2</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">3</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">4</sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Niveau sonore <sc:textLeaf role="mathtex">L</sc:textLeaf> (dB)</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">184</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">191</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">195</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">198</sc:para>
                </sc:cell>
              </sc:row>
            </sc:table>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Faut-il utiliser  la touche <sc:textLeaf role="mathtex"> \ln</sc:textLeaf> ou  <sc:textLeaf role="mathtex">\log</sc:textLeaf> sur la calculatrice ?</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Raisonnement :</sc:inlineStyle> Lorsque l'on multiplie <sc:textLeaf role="mathtex">I_1</sc:textLeaf> par <sc:textLeaf role="mathtex">n</sc:textLeaf>, cela revient à ajouter <sc:textLeaf role="mathtex">10 \log(n)</sc:textLeaf> au niveau sonore car <sc:textLeaf role="mathtex">10\log\left(\frac{nI_1}{I_0}\right)=10\log(n)+10\log\left(\frac{I_1}{I_0}\right)</sc:textLeaf></sc:para>
            <sc:para xml:space="preserve">Et on sait que <sc:textLeaf role="mathtex">10\log(2)\approx 3</sc:textLeaf> et donc <sc:textLeaf role="mathtex">10\log(4)=2\times10\log(2)\approx 6</sc:textLeaf></sc:para>
            <sc:para xml:space="preserve">On peut aussi faire tous les calculs avec la calculatrice : </sc:para>
            <sc:table role="">
              <sc:column width="23"/>
              <sc:column width="19"/>
              <sc:column width="19"/>
              <sc:column width="17"/>
              <sc:column width="17"/>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Nombre de perceuses </sc:para>
                  <sc:para xml:space="preserve">n</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">1</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">2</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">3</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">4</sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Intensité sonore <sc:textLeaf role="mathtex">\left(\mbox{W}.\mbox{m}^{-2}\right)</sc:textLeaf></sc:para>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">I_n = n \times I_1</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">1.10^{-4}</sc:textLeaf>
</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">2.10^{-4}</sc:textLeaf>
</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">3.10^{-4}</sc:textLeaf>
</sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">4.10^{-4}</sc:textLeaf></sc:para>
                </sc:cell>
              </sc:row>
              <sc:row>
                <sc:cell>
                  <sc:para xml:space="preserve">Niveau sonore L <sc:textLeaf role="mathtex">(\mbox{dB})</sc:textLeaf></sc:para>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">L = 10 \times \log \left({I \over I_0}\right)</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">L = 10 \times \log \left(\frac{1.10^{-4}} {1.10^{-12}}\right) = 80,0</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">L = 10 \times \log \left(\frac{2.10^{-4} }{1.10^{-12}}\right) = 83,0</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">L = 10 \times \log \left({3.10^{-4} \over 1.10^{-12}}\right) = 84,7</sc:textLeaf>
                  </sc:para>
                </sc:cell>
                <sc:cell>
                  <sc:para xml:space="preserve">
                    <sc:textLeaf role="mathtex">L = 10 \times \log \left({4.10^{-4} \over 1.10^{-12}}\right) </sc:textLeaf>
                    <sc:textLeaf role="mathtex">= 86,0</sc:textLeaf>
                  </sc:para>
                </sc:cell>
              </sc:row>
            </sc:table>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
