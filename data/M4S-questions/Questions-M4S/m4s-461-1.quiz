<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Trouver l'expression d'un angle ou d'une longueur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>calculette</sp:keywd>
              <sp:keywd>contexte physique</sp:keywd>
              <sp:keywd>Thales</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - JB BUTET</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">D'après le théorème de Thalès appliqué dans les deux triangles semblables OAB et OA'B'</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle relation trouve-t-on ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-461.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{\overline{OA'}}{\overline{OA}} = \frac{\overline{A'B'}}{\overline{AB}}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">bien</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{\overline{OA}}{\overline{OA'}} = \frac{\overline{AB}}{\overline{A'B'}}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Bien</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{\overline{OA'}}{\overline{OA}} = \frac{\overline{AB}}{\overline{A'B'}}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention les rapports doivent être égaux. Il faut donc que les deux plus grandes dimensions (ici OA et AB) soient toutes les deux au numérateur ou toutes les deux au dénominateur pour que la relation soit respectée.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\frac{\overline{OA}}{\overline{OA'}} = \frac{\overline{A'B'}}{\overline{AB}}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention les rapports doivent être égaux. Il faut donc que les deux plus grandes dimensions (ici OA et AB) soient toutes les deux au numérateur ou toutes les deux au dénominateur pour que la relation soit respectée.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Le théorème de Thalès repose sur une égalité des rapports des différentes longueurs des triangles.</sc:para>
            <sc:para xml:space="preserve">Il faut donc que les deux plus grandes dimensions (ici OA et AB) soient toutes les deux au numérateur ou toutes les deux au dénominateur pour que la relation soit respectée. Le rapport d'une grande longueur sur un petite ne pouvant jamais être égal au rapport d'une petite longueur sur une grande.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
