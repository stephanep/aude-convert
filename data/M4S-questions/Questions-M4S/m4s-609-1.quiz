<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Raisonner par analyse dimensionnelle</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>2</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>connaitre</sp:keywd>
              <sp:keywd>contexte physique</sp:keywd>
              <sp:keywd>Propriétés des fonctions</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - MSedzeHoo</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est la formule dimensionnellement correcte qui permet de déterminer l'énergie E en W.h stockée dans une batterie en fonction de sa tension nominale U en V et de sa charge Q en A.h ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1567415614285">manque feedback spécifique. Voir q°608 par ex</comment></comment>-->
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">E=\frac{Q}{U}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">E est en <sc:textLeaf role="mathtex">W\cdot h</sc:textLeaf> soit en <sc:textLeaf role="mathtex">V\cdot A\cdot h</sc:textLeaf>, Q en <sc:textLeaf role="mathtex">A\cdot h</sc:textLeaf> et U en V. La dimension de <sc:textLeaf role="mathtex">\frac{Q}{U}</sc:textLeaf> est <sc:textLeaf role="mathtex">\frac{A\cdot h}{V}</sc:textLeaf> ce qui n'est pas homogène avec l'énergie E.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">E=\frac{U}{Q}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">E est en <sc:textLeaf role="mathtex">W\cdot h</sc:textLeaf> soit en <sc:textLeaf role="mathtex">V\cdot A\cdot h</sc:textLeaf>, Q en <sc:textLeaf role="mathtex">A\cdot h</sc:textLeaf> et U en V. La dimension de <sc:textLeaf role="mathtex">\frac{U}{Q}</sc:textLeaf> est <sc:textLeaf role="mathtex">\frac{V}{A\cdot h}</sc:textLeaf> ce qui n'est pas homogène avec l'énergie E.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">E={Q}\times{U}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Sachant que <sc:textLeaf role="mathtex">P=U\times I</sc:textLeaf> avec P en watts (W) on peut en déduire que les watts (W) correspondent à des volts × ampères (<sc:textLeaf role="mathtex">V\cdot A</sc:textLeaf>).</sc:para>
            <sc:para xml:space="preserve">L'énergie en s'exprime donc en <sc:textLeaf role="mathtex">W\cdot h = V\cdot A\cdot h</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Or on sait que U est en V et Q en <sc:textLeaf role="mathtex">A\cdot h</sc:textLeaf> donc la formule dimensionnellement correcte est <sc:textLeaf role="mathtex">E=Q\times U</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
