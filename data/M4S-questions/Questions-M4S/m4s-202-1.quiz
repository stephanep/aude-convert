<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Déterminer l'expression littérale d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>2</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Somme de vecteurs</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Soient deux vecteurs <sc:textLeaf role="mathtex">\overrightarrow{U}</sc:textLeaf> et <sc:textLeaf role="mathtex">\overrightarrow{V}</sc:textLeaf> non nuls et a un nombre réel non nul.</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineStyle role="emp">Dans quelle situation on ne peut pas trouver a tel que <sc:textLeaf role="mathtex">\overrightarrow{U}+a.\overrightarrow{V}=\vec 0</sc:textLeaf> ?</sc:inlineStyle>
</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/m4s-202-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
Situation A
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Les deux vecteurs sont colinéaires, on va donc toujours pouvoir trouver un scalaire tel que <sc:textLeaf role="mathtex">\overrightarrow{U}+a.\overrightarrow{V}=\vec 0</sc:textLeaf>. </sc:para>
            <sc:para xml:space="preserve">Exemple : On obtient <sc:textLeaf role="mathtex">\overrightarrow{U}+a.\overrightarrow{V}</sc:textLeaf> =<sc:textLeaf role="mathtex">\overrightarrow{0}</sc:textLeaf> si a=-1</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineImg role="ico" sc:refUri="images/enonce-m4s-201-1-6.png"/>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
Situation B
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Les deux vecteurs sont colinéaires, on va donc toujours pouvoir trouver un scalaire tel que <sc:textLeaf role="mathtex">\overrightarrow{U}+a.\overrightarrow{V}=\vec 0</sc:textLeaf>. </sc:para>
            <sc:para xml:space="preserve">Exemple : On obtient <sc:textLeaf role="mathtex">\overrightarrow{U}+a.\overrightarrow{V}</sc:textLeaf> =<sc:textLeaf role="mathtex">\overrightarrow{0}</sc:textLeaf> si a=+1</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineImg role="ico" sc:refUri="images/enonce-m4s-200-1-6.png"/>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
Situation C
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Les deux vecteurs sont colinéaires, on va donc toujours pouvoir trouver un scalaire tel que <sc:textLeaf role="mathtex">\overrightarrow{U}+a.\overrightarrow{V}=\vec 0</sc:textLeaf>. </sc:para>
            <sc:para xml:space="preserve">Exemple : On obtient <sc:textLeaf role="mathtex">\overrightarrow{U}+a.\overrightarrow{V}</sc:textLeaf> =<sc:textLeaf role="mathtex">\overrightarrow{0}</sc:textLeaf> si a=+2</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineImg role="ico" sc:refUri="images/enonce-m4s-200-2-3c.png"/>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
Situation D
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Les deux vecteurs sont colinéaires, on va donc toujours pouvoir trouver un scalaire tel que <sc:textLeaf role="mathtex">\overrightarrow{U}+a.\overrightarrow{V}=\vec 0</sc:textLeaf>. </sc:para>
            <sc:para xml:space="preserve">Exemple : On obtient <sc:textLeaf role="mathtex">\overrightarrow{U}+a.\overrightarrow{V}</sc:textLeaf> =<sc:textLeaf role="mathtex">\overrightarrow{0}</sc:textLeaf> si a=-2</sc:para>
            <sc:para xml:space="preserve">
<sc:inlineImg role="ico" sc:refUri="images/enonce-m4s-201-2-4c.png"/>
</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
Situation E
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Si deux vecteurs sont colinéaires</sc:inlineStyle> (même direction), alors il sera toujours<sc:inlineStyle role="emp"> possible d'en construire une somme égale au vecteur nul</sc:inlineStyle></sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Si deux vecteurs ne sont pas colinéaires</sc:inlineStyle> (ils n'ont pas la même direction), alors il sera toujours<sc:inlineStyle role="emp"> impossible d'en construire une somme égale au vecteur nul.</sc:inlineStyle> </sc:para>
              </sc:listItem>
            </sc:itemizedList>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
