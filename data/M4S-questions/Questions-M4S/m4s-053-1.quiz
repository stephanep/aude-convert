<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Trouver l'expression d'un angle ou d'une longueur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>2</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Analyse dimensionnelle</sp:keywd>
              <sp:keywd>Surfaces et volumes</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - H. Delanoë-Ayari</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On cherche le volume d'un cône de hauteur h et de  base circulaire de rayon a .</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Parmi les formules proposées, laquelle pourrait convenir?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">V=\frac{a^2}{3h}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Pour répondre à cette question il faut raisonner par analyse dimensionnelle.</sc:para>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\left[\frac{a^2}{3h}\right]=\frac{[a^2]}{[h]}</sc:textLeaf>
            </sc:para>
            <sc:para xml:space="preserve">Si les longueurs sont exprimées en mètres alors <sc:textLeaf role="mathtex">V</sc:textLeaf> est ici exprimé en <sc:textLeaf role="mathtex">\frac{\mbox{m}^2}{\mbox{m}}=\ \mbox{m}</sc:textLeaf>, ce qui ne correspond pas à l'unité d'un volume (<sc:textLeaf role="mathtex">\ \mbox{m}^3</sc:textLeaf>)</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
V=<sc:textLeaf role="mathtex">\frac{\pi a ^ 2 h}{3}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">V=\frac{\pi a ^ 2 h ^ 2}{3}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Pour répondre à cette question il faut raisonner par analyse dimensionnelle.</sc:para>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\left[\frac{\pi a^2h^2}{3}\right]=[a^2][h^2]</sc:textLeaf>
            </sc:para>
            <sc:para xml:space="preserve">Si les longueurs sont exprimées en mètres alors <sc:textLeaf role="mathtex">V</sc:textLeaf> est ici exprimé en <sc:textLeaf role="mathtex">m^2\times m^2=m^4</sc:textLeaf>, ce qui ne correspond pas à l'unité d'un volume (<sc:textLeaf role="mathtex">\ \mbox{m}^3</sc:textLeaf>)</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">V=\frac{\pi a h}{4}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Pour répondre à cette question il faut raisonner par analyse dimensionnelle.</sc:para>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\left[\frac{\pi a h}{4}\right]=[a][h]</sc:textLeaf>
            </sc:para>
            <sc:para xml:space="preserve">Si les longueurs sont exprimées en mètres alors <sc:textLeaf role="mathtex">V</sc:textLeaf> est ici exprimé en <sc:textLeaf role="mathtex">\ \mbox{m}\times\ \mbox{m}=\ \mbox{m}^2</sc:textLeaf>, ce qui ne correspond pas à l'unité d'un volume (<sc:textLeaf role="mathtex">\ \mbox{m}^3</sc:textLeaf>)</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">V=\frac{\pi h}{a}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Pour répondre à cette question il faut raisonner par analyse dimensionnelle.</sc:para>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\left[\frac{\pi h}{a}\right]=\frac{[h]}{[a]}</sc:textLeaf>
            </sc:para>
            <sc:para xml:space="preserve">Si les longueurs sont exprimées en mètres alors V est ici exprimé en <sc:textLeaf role="mathtex">\frac{m}{m}=1</sc:textLeaf> c'est-à-dire sans unité, ce qui ne correspond pas à l'unité d'un volume (<sc:textLeaf role="mathtex">m^3</sc:textLeaf>)</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Pour répondre à cette question il faut raisonner par analyse dimensionnelle.</sc:para>
            <sc:para xml:space="preserve">La seule formule possible est : <sc:textLeaf role="mathtex">V=\frac{\pi a ^ 2 h}{3}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">En effet par analyse dimensionnelle on a : <sc:textLeaf role="mathtex">\left[\frac{\pi a^2h}{3}\right]=[a^2][h]</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Si les longueurs sont exprimées en mètres alors V est ici exprimé en <sc:textLeaf role="mathtex">\mbox{m}^2\times \mbox{m}=\mbox{m}^3</sc:textLeaf>, ce qui correspond à l'unité d'un volume (<sc:textLeaf role="mathtex">\mbox{m}^3</sc:textLeaf>).</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
