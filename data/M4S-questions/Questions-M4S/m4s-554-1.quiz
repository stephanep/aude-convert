<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Déterminer l'expression littérale d'une grandeur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L1</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF -Isoler-Extraire une grandeur</sp:keywd>
              <sp:keywd>Contexte - physique</sp:keywd>
              <sp:keywd>Procédés réciproques</sp:keywd>
              <sp:keywd>maitrise:intermédiaire</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Richard Dupont complété par Hélène Dechelette</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On considère l'écoulement gravitaire d'une conduite de diamètre D, de longueur L et de hauteur h. Le débit est Q, le coefficient de perte de charge régulière est λ , l'accélération de la pesanteur est g.</sc:para>
            <sc:para xml:space="preserve">On sait que : <sc:textLeaf role="mathtex">h=\frac{8\lambda LQ^2}{g\pi ^2D^5}</sc:textLeaf>.
</sc:para>
          </op:txt>
        </sp:txt>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est l'expression de D ?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">D = <sc:textLeaf role="mathtex">\sqrt[5]{{8\lambda L Q^2}-{h \pi^2  g}}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Il faut diviser et non soustraire pour résoudre l'équation</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">D = <sc:textLeaf role="mathtex">\sqrt{\frac{8\lambda L Q^2}{h \pi^2  g}}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">ce n'est pas une simple racine carrée</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">D= <sc:textLeaf role="mathtex">\sqrt[5]{\frac{8\lambda L Q^2}{h \pi^2  g}}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Raisonnement </sc:inlineStyle>: D'après l'énoncé on sait que : <sc:textLeaf role="mathtex">h=\frac{8\lambda LQ^2}{g\pi ^2D^5}</sc:textLeaf> et on cherche l'expression de D.</sc:para>
            <sc:para xml:space="preserve"><sc:inlineStyle role="emp">Calcul littéral</sc:inlineStyle> : </sc:para>
            <sc:itemizedList>
              <sc:listItem>
                <sc:para xml:space="preserve">On se ramène à une équation en ligne : <sc:textLeaf role="mathtex">h\cdot g\pi ^2D^5=8\lambda LQ^2</sc:textLeaf></sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">On isole <sc:textLeaf role="mathtex">D^5</sc:textLeaf> en divisant par ce qui est en facteur : <sc:textLeaf role="mathtex">D^5=\frac{8\lambda LQ^2}{hg\pi ^2}</sc:textLeaf>.</sc:para>
              </sc:listItem>
              <sc:listItem>
                <sc:para xml:space="preserve">On obtient <sc:textLeaf role="mathtex">D</sc:textLeaf> en utilisant la fonction <sc:textLeaf role="mathtex">(...)^{1/5}</sc:textLeaf> qui est la fonction réciproque de la fonction <sc:textLeaf role="mathtex">(...)^5</sc:textLeaf> : <sc:textLeaf role="mathtex">D=\left(\frac{8\lambda L Q^2}{h \pi^2 g}\right)^{{1}/{5}}</sc:textLeaf> </sc:para>
              </sc:listItem>
            </sc:itemizedList>
            <sc:para xml:space="preserve">Ce que l'on peut réécrire aussi : <sc:textLeaf role="mathtex">D=\sqrt[5]{\frac{8\lambda L Q^2}{h \pi^2 g}}</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
