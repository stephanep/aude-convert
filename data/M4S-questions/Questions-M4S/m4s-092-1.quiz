<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Modéliser une grandeur physique avec un vecteur</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#math-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>Pythagore</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Cécile Le Luyer et Jeanne Parmentier</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Une personne part de chez elle (point O) et se déplace pour travailler en voiture, de 30 km vers l'ouest, puis de 60 km vers le nord. Elle arrive au point M. </sc:para>
            <sc:para xml:space="preserve">
 <sc:inlineStyle role="emp">Quelle sont les expressions des coordonnées et de la norme de son vecteur position <sc:textLeaf role="mathtex"> \overrightarrow{OM}</sc:textLeaf> dans le repère<sc:textLeaf role="mathtex"> (O, \overrightarrow{i} , \overrightarrow{j})</sc:textLeaf>?</sc:inlineStyle></sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-064-1.png">
          <op:resInfoM/>
        </sp:res>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{OM} (-30, 60)</sc:textLeaf> et <sc:textLeaf role="mathtex">|| \overrightarrow{OM} || = \sqrt{(-30)^2 + (60)^2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{OM} (30, 60)</sc:textLeaf> et <sc:textLeaf role="mathtex">|| \overrightarrow{OM} || = \sqrt{(30)^2 + (60)^2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention, les axes sont orientés, et aller vers l'ouest signifie aller “à gauche”, c'est à dire vers les x négatifs.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{OM} (60, 30)</sc:textLeaf> et <sc:textLeaf role="mathtex">|| \overrightarrow{OM} || = \sqrt{(60)^2 + (30)^2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Les axes sont orientés, attention à ne pas confondre l'axe des abscisses et l'axe des ordonnées.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">\overrightarrow{OM} (-60, 30)</sc:textLeaf> et <sc:textLeaf role="mathtex"> ||\overrightarrow{OM} || = \sqrt{(-60)^2 + (30)^2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Attention, les axes sont orientés et aller vers l'ouest signifie aller “à gauche”, c'est à dire vers les x négatifs.</sc:para>
            <sc:para xml:space="preserve">Aller au nord signifie aller “en haut” c'est à dire, avoir une composante suivant y positive.</sc:para>
            <sc:para xml:space="preserve">Attention également à ne pas confondre l'axe des abscisses et l'axe des ordonnées.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Un déplacement de 30 km vers l'ouest correspond à une projection de -30 suivant l'axe des abscisses, tandis que 60 km vers le nord correspond à une projection de 60 suivant l'axe des ordonnées. Au final,  <sc:textLeaf role="mathtex">\overrightarrow{OM} (-30, 60)</sc:textLeaf> et <sc:textLeaf role="mathtex">|| \overrightarrow{OM} || = \sqrt{(-30)^2 + (60)^2}</sc:textLeaf>
</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
