<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Estimer les paramètres d'un modèle</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF - fonction affine-équation droite-graphe</sp:keywd>
              <sp:keywd>Contexte - physique</sp:keywd>
              <sp:keywd>Équation caractéristique</sp:keywd>
              <sp:keywd>Pente</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Richard Dupont</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">On a mesuré la masse m de différentes pièces de bois de volume V. Il est alors facile de déterminer la masse volumique <sc:textLeaf role="mathtex">\rho</sc:textLeaf> du bois par l'expression <sc:textLeaf role="mathtex">m = \rho V.</sc:textLeaf>
</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-236-4.png">
          <op:resInfoM/>
        </sp:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">La masse volumique <sc:textLeaf role="mathtex">\rho</sc:textLeaf> du bois est de:</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">50 g/cm<sc:textLeaf role="exp">3</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Revoir la définition de la pente de la droite. Il faut diviser par la variation de volume.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">0,5 g/cm<sc:textLeaf role="exp">3</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">2 g/cm<sc:textLeaf role="exp">3</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Inversion de la pente. La pente est la valeur du quotient de la variation verticale (m) sur la variation horizontale (V) et non l'inverse.</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\frac{50}{100}\ \mbox{g/cm}^3</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
              <sc:textLeaf role="mathtex">\frac{25}{50}\ \mbox{g/cm}^3</sc:textLeaf>
            </sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <!--<comment xmlns="scenari.eu:comment:1.0" type="thread" threadClosed="true"><comment author="auc" creationTime="1567001193160">à finir de rédiger
fractions en fractions latex</comment></comment>-->
          <op:txt>
            <sc:para xml:space="preserve">La masse volumique est la pente de la droite représentative de la fonction m (masse) de variable V (volume) telle que : <sc:textLeaf role="mathtex">m = \rho V</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Il faut calculer cette pente à l'aide des coordonnées des 2 points présentés sur le schéma. Si la droite  de pente <sc:textLeaf role="mathtex">\rho</sc:textLeaf> passe par deux points A(V<sc:textLeaf role="ind">A</sc:textLeaf> ; m<sc:textLeaf role="ind">A</sc:textLeaf>) et B(V<sc:textLeaf role="ind">B</sc:textLeaf> ; m<sc:textLeaf role="ind">B</sc:textLeaf>), par définition :  <sc:textLeaf role="mathtex">\rho=\frac{m_B-m_A}{ V_B-V_A}</sc:textLeaf>.</sc:para>
            <sc:para xml:space="preserve">Ainsi on a : <sc:textLeaf role="mathtex">\rho=\frac{50-0}{100-0}=\frac{50}{100}=\frac{25}{50}=0,5 \ \mbox{g/cm}^3</sc:textLeaf>.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
