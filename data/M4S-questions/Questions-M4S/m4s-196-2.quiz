<?xml version='1.0' encoding='UTF-8'?>
<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
  <op:mcqMur xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    <op:exeM>
      <sp:title>Estimer les paramètres d'un modèle</sp:title>
      <sp:themeLicence>#phys-mathphys-</sp:themeLicence>
      <sp:themeLicence>#phys-</sp:themeLicence>
      <sp:level>3</sp:level>
      <sp:educationLevel>L0</sp:educationLevel>
      <sp:info>
        <op:info>
          <sp:keywds>
            <op:keywds>
              <sp:keywd>SF - fonction trigo-graphe</sp:keywd>
              <sp:keywd>Contexte - physique</sp:keywd>
              <sp:keywd>Équation caractéristique</sp:keywd>
              <sp:keywd>maitrise:en surface</sp:keywd>
            </op:keywds>
          </sp:keywds>
          <sp:cpyrgt>
            <op:sPara>
              <sc:para xml:space="preserve">Projet M4S - Richard Dupont</sc:para>
            </op:sPara>
          </sp:cpyrgt>
        </op:info>
      </sp:info>
    </op:exeM>
    <sc:question>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">L'enregistrement d'un son délivré par un diapason est donné par le graphe suivant :</sc:para>
          </op:txt>
        </sp:txt>
        <sp:res sc:refUri="images/enonce-m4s-196-2.png">
          <op:resInfoM/>
        </sp:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">L'évolution temporelle du signal sonore peut être modélisée par l'expression <sc:textLeaf role="mathtex">U(t) = U_{max} \sin ( \omega t)</sc:textLeaf>
</sc:para>
            <sc:para xml:space="preserve">avec U<sc:textLeaf role="ind">max</sc:textLeaf> la tension maximale (unité : volt V) délivrée par le diapason, <sc:textLeaf role="mathtex">\omega</sc:textLeaf> la pulsation du signal et t le temps .</sc:para>
            <sc:para xml:space="preserve">
              <sc:inlineStyle role="emp">Quelle est l'expression correcte de U(t)?</sc:inlineStyle>
            </sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:question>
    <sc:choices>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">U(t) = 5 \times \sin (2000\pi t)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Revoir l'évaluation de <sc:textLeaf role="mathtex">\omega</sc:textLeaf> ( et de T ?)</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">U(t) = 10 \times \sin (1000\pi t)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Revoir l'amplitude : ce n'est pas la différence entre le maximum et le minimum , et <sc:textLeaf role="mathtex">\omega</sc:textLeaf> (et de T ?)</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">U(t) = 5\times \sin (\frac{2000}{3}\pi t)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">Revoir l'évaluation de <sc:textLeaf role="mathtex">\omega</sc:textLeaf> (et de T ?)</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="unchecked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">U(t) = 10\times \sin (\frac{4000}{3}\pi t)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
        <sc:choiceExplanation>
          <op:txt>
            <sc:para xml:space="preserve">revoir la définition de l'amplitude, ce n'est pas la différence entre le maximum et le minimum</sc:para>
          </op:txt>
        </sc:choiceExplanation>
      </sc:choice>
      <sc:choice solution="checked">
        <sc:choiceLabel>
          <op:txt>
            <sc:para xml:space="preserve">
<sc:textLeaf role="mathtex">U(t) = 5\times \sin (\frac{4000}{3}\pi t)</sc:textLeaf>
</sc:para>
          </op:txt>
        </sc:choiceLabel>
      </sc:choice>
    </sc:choices>
    <sc:globalExplanation>
      <op:res>
        <sp:txt>
          <op:txt>
            <sc:para xml:space="preserve">Connaître la définition de l'amplitude du signal.</sc:para>
            <sc:para xml:space="preserve">Savoir que <sc:textLeaf role="mathtex">\omega = \frac{2 \times\pi}{T}</sc:textLeaf> et calculer la valeur de ce paramètre à partir de celle de la période relevée sur le graphe : T=1,5 ms.</sc:para>
            <sc:para xml:space="preserve">Attention le temps est mesuré en ms.</sc:para>
          </op:txt>
        </sp:txt>
      </op:res>
    </sc:globalExplanation>
  </op:mcqMur>
</sc:item>
