#!/usr/bin/python3


'''
audeconvert

Change metadata to normalize m4s quiz into the unisciel / f2s format

@author:     Stéphane Poinsart
@copyright:  Université de Technologie de Compiègne, Cellule d'Appui Pédagogique
@license:    GPL/LGPL/CECL/MPL
@contact:    stephane.poinsart@utc.fr
'''

import sys
import os
import shutil
import argparse
import unicodedata
import re
import traceback
import math

from lxml import etree

import lxml.html
# import lxml.html.soupparser

from lxml.builder import ElementMaker

from enum import Enum

from os.path import dirname

import csv

import urllib
import base64

import html

import yaml
import pprint
import unicodedata

pp = pprint.PrettyPrinter(indent=4)


#import pydevd
#pydevd.settrace("localhost", port=5678)



NSmaps={
    'xml': 'http://www.w3.org/XML/1998/namespace',
    'sc' : 'http://www.utc.fr/ics/scenari/v3/core',
    'op' : 'utc.fr:ics/opale3',
    'sp' : 'http://www.utc.fr/ics/scenari/v3/primitive'
}

from enum import Enum


XMLneutral = ElementMaker()
XMLsc = ElementMaker(namespace=NSmaps['sc'], nsmap={'sc': NSmaps['sc']})
XMLop = ElementMaker(namespace=NSmaps.get('op'), nsmap={'op': NSmaps.get('op')})
XMLsp = ElementMaker(namespace=NSmaps.get('sp'), nsmap={'sp': NSmaps.get('sp')})
XMLopsp = ElementMaker(namespace=NSmaps.get('op'), nsmap={'op': NSmaps.get('op'), 'sp': NSmaps.get('sp')})


maincall = argparse.ArgumentParser(description='Convert from m4s to f2s')
maincall.add_argument('csvfile', help='csv input file to match nodelets with tags')
maincall.add_argument('datadir', help='apply changes on this directory')
options = maincall.parse_args()

parser = etree.XMLParser(remove_blank_text=True, remove_comments=False, recover=True)

if (not os.path.isfile(options.csvfile)):
    sys.stderr.write('Error: "'+options.csvfile+'" csvfile is not a file\n')
    sys.exit(1)
csvfile = os.path.realpath(options.csvfile)

if (not os.path.isdir(options.datadir)):
    sys.stderr.write('Error: "'+options.datadir+'" is not a directory"\n')
    sys.exit(1)
datadir = os.path.realpath(options.datadir)


# question id counter (to ensure uniqueness)
qcount=0
imgcount=0
xmlcount=0

debugtitle=""
themeindex=None
themelyceeindex=None
csvdata=None
alreadydone=set()

def useless(str):
    return (str is None or str=='' or str.isspace())
    

def errormessage(message):
    if (debugtitle is not None):
        print("Question "+str(xmlcount)+" \""+debugtitle+"\": "+message)
    else:
        print("Question "+str(xmlcount)+" non chargée: "+message)

def nstag(prefix, tag):
    return "{"+NSmaps[prefix]+"}"+tag;

nslist={
    "sc":"http://www.utc.fr/ics/scenari/v3/core",
    "op":"utc.fr:ics/opale3",
    "sp":"http://www.utc.fr/ics/scenari/v3/primitive",
    }
# return element tags without python formated namespaces
def stripNs(s):
    return s.split('}', 1)[1]

# return element tags without python formated namespaces
def longNs(s):
    splited=s.split(':')
    if (len(splited)!=2):
        errormessage('invalid NS+tag: '+s)
    ns, tag=splited
    return '{'+nslist[ns]+'}'+tag

def loadcsv(csvfile):
    csvdata={}
    with open(csvfile) as f:
        for row in csv.DictReader(f, skipinitialspace=True):
            csvdata[row['file']]=row
    return csvdata

def loadthemefile(themefilename, index, codearray):
    themeNSmap={
        'sm': 'http://www.utc.fr/ics/scenari/v3/modeling',
    }
    # read the theme list file
    themefile=os.path.join(os.path.dirname(os.path.realpath(__file__)), themefilename)
    if (not themefile or not os.path.isfile(themefile)):
        errormessage("impossible de trouver les fichiers de liste des thèmes"+themefilename)
        return None
    themetree = etree.parse(themefile, parser)
    if (themetree is None):
        errormessage("impossible d'ouvrir les fichiers de liste des thèmes "+themefilename)
        return None

    for theme in themetree.findall('.//sm:option', namespaces=themeNSmap):
        code=theme.get("key")
        text=theme.get("name")
        # swap the hash table key/value pair and clean the index to reduce data entry error
        index[cleantheme(text)]=code
        codearray.append(cleancode(code))

    return index, codearray


# load theme file from topoze source code and return a theme index
# topoze/sources/topoze/gen/mirage/svc/svcDistrib.doss/quizConfig.js
# it's javascript and not pure json, but we can manage around that
def loadthemes():
    themecodearray=[]
    themelyceecodearray=[]
    themeindex={}
    themelyceeindex={}

    themeindex,themecodearray=loadthemefile("themeLicence.xml", themeindex, themecodearray)
    lthemelyceeindex,themelyceecodearray=loadthemefile("themeLycee.xml", themelyceeindex, themelyceecodearray)

    return themeindex, themecodearray, themelyceeindex, themelyceecodearray

def cleancode(text):
    return text.strip().lower()

def cleantheme(text):
    text=''.join((c for c in unicodedata.normalize('NFD', text) if unicodedata.category(c) != 'Mn'))
    text=''.join(e for e in text  if (e.isalnum() or ord(e)==ord(' ')))
    return text.strip().lower()


# check the image and copy it in the right path
def image(uri, caller):
    return os.path.join('img', newfilename)

# XML tool: delete all attributes of an element
def delete_attributes(element):
    for key in element.keys():
        del element.attrib[key]


# parse Field quiz type
def parsefield(item, itemtype):
    global debugtitle
    
    debugtitle=title=getvalidtext(item.find('.//name/text'))
    xmlmeta=filtermeta(item)
    
    question=filtercontent(item.findtext('.//questiontext/text'), contentlvl.FLOWALL)
    writeallfiles(item.findall('.//questiontext/file'))
    xmlquestion=None
    if (question is not None):
        xmlquestion=XMLsc.question(question)
    
    globalexplanation=filtercontent(item.findtext('.//generalfeedback/text'), contentlvl.FLOWALL)
    writeallfiles(item.findall('.//generalfeedback/file'))
    xmlglobalexplanation=None
    if (globalexplanation is not None):
        xmlglobalexplanation=XMLsc.globalExplanation(globalexplanation)
 
    writeallfiles(item.findall('.//answer/file'))

    # xml output vary slightly depending on quiz type
    if (itemtype=='shortanswer'):
        xmlquiz=XMLop.field()
        xmlsolution=XMLsc.SFSolution()
    else:
        xmlquiz=XMLop.numerical()
        xmlsolution=XMLsc.NFSolution()

    # handle possible solutions
    solutioncount=0
    
    for answer in item.findall('.//answer[@fraction=\'100\']'):
        solution=answer.findtext("./text")
        if (solution):
            xmlsolution.append(XMLsc.value(solution))
            solutioncount+=1
        tolerance=answer.findtext("./tolerance")
        if (tolerance):
            if (not tolerance.isnumeric()):
                errormessage("Question numérique avec tolérance qui ne semble pas une valeur numérique : \""+tolerance+"\".")
            xmlsolution.append(XMLsc.condition(tolerance, operation="+-"))

    
    if (solutioncount<1):
        errormessage("L'exercice n'a aucune solution proposée")
    if (solutioncount>1 and itemtype=='numerical'):
        errormessage("Question numérique avec plusieurs solutions, Scenari ne supporte qu'une seule solution, les suivantes seront en erreur")
    
    xmlasw=XMLsp.asw(xmlsolution)
    for element in [xmlmeta, xmlquestion, xmlasw, xmlglobalexplanation]:
        if (element is not None):
            xmlquiz.append(element)
    
    return xmlquiz

# Transform a workshop path to a filesystem path
def workpath(path, caller):
    # absolute path : just remove the /
    if (path.startswith('/')):
        return(os.path.join(datadir, path[1:]))
        
    # relative path : join with the caller path, if we have one
#    if ((not caller) or (not os.path.dirname(caller))):
        #return os.path.join(inputdir, path)
    
    callerdir=os.path.dirname(caller)
    if callerdir.startswith('/'):
        callerdir=callerdir[1:]
#    print('workpath: path='+path +', caller=' + caller)
#    print('workpath: ret='+os.path.normpath(os.path.join(inputdir, callerdir, path)))
#    traceback.print_stack()
    return os.path.normpath(os.path.join(datadir, callerdir, path))

# Transform a relative workshop path to an absolute workshop path 
def absolutize(path, caller):
    fspath=workpath(path, caller)
    fspath=os.path.normpath(fspath)
#    print('absolutize -> path='+path+', caller='+caller);
#    print('absolutize -> return '+os.path.relpath(fspath, inputdir));
    return '/'+os.path.relpath(fspath, datadir)

def usableFile(filename, caller):
    filename=workpath(filename, caller)
    # check if the file we open exists
    if (not os.path.isfile(filename)):
        sys.stderr.write('Warning: "'+str(filename)+'" referenced from "'+str(caller)+'" is not a file, ignoring\n')
        #traceback.print_stack()
        return False
    
    #check if the file we open is in the real path
    filename = os.path.realpath(filename)
    if (not filename.startswith(os.path.realpath(datadir))):
        sys.stderr.write('Warning: "'+str(filename)+'" referenced from "'+str(caller)+'" is not contained in the workshop path, ignoring\n')
        return False
    return True

class XMLChange(Enum):
    INSERT=1    # add new xml element, if it does not exists with both same tag and same value. Place it after the element with the same tag but a different value. If it does not exist, create the tree after the first previous valid element
    UPDATE=2    # change the content of the node, creating it if necessary
    DELETE=4    # delete the node

def pathisparent(parentpath, elementpath):
    if (not parentpath or not elementpath):
        return false
    if (parentpath[-1]!='/'):
        parentpath+='/'
    return elementpath.startswith(parentpath)

def removeprefix(s, prefix):
    if s.startswith(prefix):
        return s[len(prefix):]
    else:
        return s

# update from a data line when the path does not already exist (so we need to work harder to place the new element)
# problems that need to be handled :
# * the node that we need to set does not exists
# * the parent, grandparent, grandgrand(...) of that node does not exists
# * when we find one that exists, we need to add the child at the right place, if not we fail validation
def xmlupdatefromline(e, data, line, caller):
#    print('testing implementation of xmlupdatefromline, to add '+line['path'] + " with text "+str(line['text']))
    # looking for parents of the current element
    lastparent=None
    lastsibling=None
    curpath=line['path']
    for iterline in data:
        iterpath=iterline['path']
        # if we find a path that exist, we check it to know if it's the best existing parent or sibling
        if e.find(iterpath, nslist) is not None:
            # the tentative path is more precise that what we already have as a lastparent (if it's less precise, it's of no use)
            if (lastparent is None or len(iterpath)>len(lastparent)):
                # if the tentative path is a parent, then it's our new best parent
                if (pathisparent(iterpath, curpath)):
                    lastparent=iterpath
                    lastsibling=None
                else:
                    lastsibling=iterpath
    # compute tags that need to be created between parent and final leaf tag (it can be many)
    deltatags=removeprefix(curpath, lastparent).split('/')
    deltatree=None
    lastnode=None
    for tag in deltatags:
        if (tag):
            newnode=etree.Element(longNs(tag))
            if (lastnode is not None):
                lastnode.append(newnode)
            else:
                deltatree=newnode
            lastnode=newnode
    lastnode.text=str(line['text'])
                
#    print("\n__________ CREATED DELTATREE: "+removeprefix(curpath, lastparent) +" to " +etree.tostring(deltatree, xml_declaration=True, encoding='utf-8', pretty_print=True).decode("utf-8")+"\n")
    if lastsibling is not None and (allsiblingnodes := e.findall(lastsibling, nslist)) is not None:
        lastsiblingnode=allsiblingnodes[-1]
        lastsiblingnode.addnext(deltatree)
    else:
        parentnode=e.find(lastparent, nslist)
        parentnode.insert(0, deltatree)
            
    return e


# data = [ {'path':'/sc:../op:exeM/sp:level', 'text':3, 'change':XMLChange.UPDATE, 'changefornone':true}, {...} ]
# path : path where the element should be changed
# value : value in element text node
# change : what opperation should we run
# changefornone : make the change even if the value is null or empty (value of 0 will still run the change no matter what)
def xmlupdatefromarray(e, data, caller):
    insertpoint=None
#    print("\n>>>>>>> BEFORE\n" +etree.tostring(e, xml_declaration=True, encoding='utf-8', pretty_print=True).decode("utf-8")+"\n>>>>>>>>>\n")
    for line in data:
#        print(line)
        # ignore the line if it exist only for positionnal hinting
        if ('text' not in line or 'path' not in line or not line['text']):
            continue
        samenodes=e.findall(line['path'], nslist)
        if (samenodes):
            # if node exist with same content, no need to add twice, skip the line
            for node in samenodes:
                if (node.text.strip()==str(line['text'])):
                    line=None
                    break
            if (line==None):
                continue
            
            lastsibling=samenodes[-1]
            # change the content
            if (line['change']==XMLChange.UPDATE):
                lastsibling.text=str(line['text'])
            # even if it exists, for an insert we need to make a new one as a sibling
            elif (line['change']==XMLChange.INSERT):
                lastpathcomp=line['path'].split('/')[-1]
                newnode=etree.Element(longNs(lastpathcomp))
                newnode.text=str(line['text'])
                lastsibling.addnext(newnode)
        else:
            e=xmlupdatefromline(e, data, line, caller)
#    print("\n<<<<<<< AFTER\n" +etree.tostring(e, xml_declaration=True, encoding='utf-8', pretty_print=True).decode("utf-8")+"\n<<<<<<<<<\n")
    return e



def parseQuiz(e, inputfile, filetype, caller):
    global csvdata

    csvline=None
    basename=os.path.basename(caller)
    if (basename in csvdata):
        csvline=csvdata[basename]
    else:
        errormessage("node file "+basename+" is currently used, but not listed in csv file")
    
    # example : representation-2A-M.nodelet
    #           .*-(bloom)(maitrise)-(M=math/PC=physique).nodelet
    nameinfo = re.search("-([1-3])([A-C])(-([MPC]+))?.nodelet", caller)

    if ('title' in csvline and csvline['title']):
        title=csvline['title']
    else:
        title=None
    if ('keyword1' in csvline and csvline['keyword1']):
        keyword1=csvline['keyword1']
    else:
        keyword1=None
    if ('keyword2' in csvline and csvline['keyword2']):
        keyword2=csvline['keyword2']
    else:
        keyword2=None

    bloom, maitrise, void, domaine=nameinfo.groups()
#    print('bloom: '+bloom+', maitrise: '+maitrise+', domaine: '+domaine)
    xmlthemelist=e.findall('.//op:exeM/sp:themeLicence',nslist)
    xmlbloom=e.find('.//op:exeM/sp:level',nslist)

    data=[]
    data.append({'path':'.//op:exeM'})
    data.append({'path':'.//op:exeM/sp:title', 'text':title, 'change':XMLChange.UPDATE})
    data.append({'path':'.//op:exeM/sp:themeLycee'})
    themelicencevalue=None
    if (domaine=='M'):
        themelicencevalue='#math-'
    elif (domaine=='PC'):
        themelicencevalue='#phys-'
    data.append({'path':'.//op:exeM/sp:themeLicence', 'text':themelicencevalue, 'change':XMLChange.INSERT})

    data.append({'path':'.//op:exeM/sp:conceptInventory'})
    
    bloomvalue=None
    if (bloom=='1'):
        bloomvalue=1
    elif (bloom=='2'):
        bloomvalue=2
    elif (bloom=='3'):
        bloomvalue=3
    if (maitrise=='C'):
        bloomvalue=4
    data.append({'path':'.//op:exeM/sp:level', 'text':bloomvalue, 'change':XMLChange.UPDATE})

    data.append({'path':'.//op:exeM/sp:educationLevel'})

    # maitrise
    data.append({'path':'.//op:exeM/sp:info'})
    data.append({'path':'.//op:exeM/sp:info/op:info'})
    data.append({'path':'.//op:exeM/sp:info/op:info/sp:keywds'})
    data.append({'path':'.//op:exeM/sp:info/op:info/sp:keywds/op:keywds'})

    maitrisekeywordvalue=None
    if (maitrise=='A'):
        maitrisekeywordvalue='maitrise:en surface'
    elif (maitrise=='B'):
        maitrisekeywordvalue='maitrise:intermédiaire'
    if (keyword1):
        data.append({'path':'.//op:exeM/sp:info/op:info/sp:keywds/op:keywds/sp:keywd', 'text':keyword1, 'change':XMLChange.INSERT})
    if (keyword2):
        data.append({'path':'.//op:exeM/sp:info/op:info/sp:keywds/op:keywds/sp:keywd', 'text':keyword2, 'change':XMLChange.INSERT})
    data.append({'path':'.//op:exeM/sp:info/op:info/sp:keywds/op:keywds/sp:keywd', 'text':maitrisekeywordvalue, 'change':XMLChange.INSERT})


    e=xmlupdatefromarray(e, data, caller)
    return e;
#    print("\n>>>>> WRITE TO FILE >>>>> " + workpath(inputfile, caller)+"\n"+etree.tostring(e, xml_declaration=True, encoding='utf-8', pretty_print=True).decode("utf-8")+"\n\n")
#    etree.ElementTree(e).write(workpath(inputfile, caller), xml_declaration=True, encoding='utf-8', pretty_print=True)

#    print(nameinfo.groups())


# Parse nodelet with as a generic Scenari XML file to find references to quiz files directly under it
def parseNodelet(e, inputfile, filetype):
    for ref in e.iterfind('.//*[@sc:refUri]',nslist):
        newfile=ref.attrib['{'+nslist['sc']+'}refUri']
        if (newfile.endswith('.quiz')):
            parseFile(absolutize(newfile, inputfile), inputfile)

# Load and parse any XML file and route it to the appropriate handler (XML or quiz)
def parseFile(inputfile, caller=None):
    global alreadydone
    # dont change the same quiz twice
    if (inputfile in alreadydone):
        errormessage("Quiz file "+inputfile+" has been referenced more than once, skipping extra times")
        return
    alreadydone.add(inputfile)

    if (caller is None):
        inputfile='/'+os.path.relpath(inputfile, datadir)
    
    effectivefile=workpath(inputfile, caller)
    
    if (caller and not usableFile(inputfile, caller)):
        return
    
    tree = etree.parse(effectivefile, parser)
    if (not tree):
        print('Error: XML from "'+effectivefile+'" not loaded correctly (parse)')
        return
    root = tree.getroot()
    if (not tree):
        print('Error: XML from "'+effectivefile+'" not loaded correctly (getroot)')
        return
    
    filetype = stripNs(root[0].tag)


    if (filetype=='assmntNodelet'):
        print('nodelet: '+inputfile);
        parseNodelet(root[0], inputfile, filetype)
    # supported quiz type
    elif (inputfile.endswith('.quiz')):
        print('   quiz: '+inputfile);
        if (parseQuiz(root[0], inputfile, filetype, caller)):
            tree.write(workpath(inputfile, caller), xml_declaration=True, encoding='utf-8', pretty_print=True)
    # other XML file that is not a quiz
    else:
        print('Warning: unsupported file "'+inputfile+'"')





def findnodelets(csvfile, datadir):
    global csvdata
    csvdata=loadcsv(csvfile)
#    print("loaded csvdata:\n"+str(csvdata)+"\n")
    for root, dirs, files in os.walk(datadir):
        for f in files:
            if (f.endswith(".nodelet")):
#                print(f)
                parseFile(os.path.join(root, f))


themeindex,themecodearray,themelyceeindex,themelyceecodearray=loadthemes()
findnodelets(csvfile, datadir)


#print(etree.tostring(injectpara(etree.fromstring(xmldata,parser)), xml_declaration=True, encoding='utf-8', pretty_print=True).decode("utf-8"))


